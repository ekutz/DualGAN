#!/bin/sh
#dcgan
python /Users/eike/DTU/dcgan_pytorch/stage1/main_deep_pac.py  --model_path=/Users/eike/DTU/Evaluation/FID_calculation/gland/models/Models/s1/Jul21_20-05-16/12000 --inf_out=/Users/eike/DTU/Evaluation/FID_calculation/gland/interpolation/dcgan --gen_amount=10 --dataset=gland --ngf=64 --mode=interpolate_latent_space --image_size=256 --nc=1

#dcgan_pac2
python /Users/eike/DTU/dcgan_pytorch/stage1/main_deep_pac.py  --model_path=/Users/eike/DTU/Evaluation/FID_calculation/gland/models/Models/s1/Jul22_13-05-03/1000 --inf_out=/Users/eike/DTU/Evaluation/FID_calculation/gland/interpolation/dcgan_pac2 --gen_amount=10 --dataset=gland --ngf=64 --mode=interpolate_latent_space --image_size=256 --nc=1

#sagan
python /Users/eike/DTU/dcgan_pytorch/stage1/main_sagan_pac.py  --model_path=/Users/eike/DTU/Evaluation/FID_calculation/gland/models/Models/s1/Jul17_16-14-20/11000 --inf_out=/Users/eike/DTU/Evaluation/FID_calculation/gland/interpolation/sagan --gen_amount=10 --dataset=gland --ngf=32 --mode=interpolate_latent_space --image_size=256 --nc=1
#sagan_pac2
python /Users/eike/DTU/dcgan_pytorch/stage1/main_sagan_pac.py  --model_path=/Users/eike/DTU/Evaluation/FID_calculation/gland/models/Models/s1/Jul20_09-27-57/12000 --inf_out=/Users/eike/DTU/Evaluation/FID_calculation/gland/interpolation/sagan_pac2 --gen_amount=10 --dataset=gland --ngf=32 --mode=interpolate_latent_space --image_size=256 --nc=1
#sagan_pac5
python /Users/eike/DTU/dcgan_pytorch/stage1/main_sagan_pac.py  --model_path=/Users/eike/DTU/Evaluation/FID_calculation/gland/models/Models/s1/Jul19_15-45-49/6000 --inf_out=/Users/eike/DTU/Evaluation/FID_calculation/gland/interpolation/sagan_pac5 --gen_amount=10 --dataset=gland --ngf=32 --mode=interpolate_latent_space --image_size=256 --nc=1
#sagan_hb
python /Users/eike/DTU/dcgan_pytorch/stage1/main_sagan_pac.py  --model_path=/Users/eike/DTU/Evaluation/FID_calculation/gland/models/Models/s1/Jul19_16-27-03/12000 --inf_out=/Users/eike/DTU/Evaluation/FID_calculation/gland/interpolation/sagan_hb --gen_amount=10 --dataset=gland --ngf=32 --mode=interpolate_latent_space --image_size=256 --nc=1
#sagan_pac2_hb
python /Users/eike/DTU/dcgan_pytorch/stage1/main_sagan_pac.py  --model_path=/Users/eike/DTU/Evaluation/FID_calculation/gland/models/Models/s1/Jul21_09-53-13/12000 --inf_out=/Users/eike/DTU/Evaluation/FID_calculation/gland/interpolation/sagan_pac2_hb --gen_amount=10 --dataset=gland --ngf=32 --mode=interpolate_latent_space --image_size=256 --nc=1

