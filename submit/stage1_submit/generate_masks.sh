#!/bin/sh
### General options
### –- specify queue --
#BSUB -q gpuv100
### -- set the job Name --
#BSUB -J gld_dc_pac
### -- ask for number of cores (default: 1) --
#BSUB -n 1
### -- Select the resources: 1 gpu in exclusive process mode --
#BSUB -gpu "num=1:mode=exclusive_process"
### -- set walltime limit: hh:mm --  maximum 24 hours for GPU-queues right now
#BSUB -W 1:00
# request 5GB of system-memory
#BSUB -R "rusage[mem=128GB]"
# select the amount of GPU memory needed
#BSUB -R "select[gpu32gb]"
### -- set the email address --
# please uncomment the following line and put in your e-mail address,
# if you want to receive e-mail notifications on a non-default address
##BSUB -u s182902@student.dtu.dk
### -- send notification at start --
#BSUB -B
### -- send notification at completion--
#BSUB -N
### -- Specify the output and error file. %J is the job-id --
### -- -o and -e mean append, -oo and -eo mean overwrite --
#BSUB -o logs/gpu-%J.out
#BSUB -e logs/gpu_%J.err
# -- end of LSF options --

nvidia-smi
# Load the cuda module
module load cuda/10.2
module load python3/3.7.5

/appl/cuda/10.2/samples/NVIDIA_CUDA-10.2_Samples/bin/x86_64/linux/release/deviceQuery


#gland

#dcgan
#python3 stage1/main_deep_pac.py --dataset=gland --ngf=64 --mode=inference --gen_amount=100 --image_size=256 --nc=1 --model_path=/zhome/95/c/135723/Models/s1/Jul21_20-05-16/12000 --inf_out=/zhome/95/c/135723/FID_calculation/gland/data/stage1/dcgan/img
#dcgan_pac2
#python3 stage1/main_deep_pac.py --dataset=gland --ngf=64 --mode=inference --gen_amount=100 --image_size=256 --nc=1 --model_path=/zhome/95/c/135723/Models/s1/Jul22_13-05-03/1000 --inf_out=/zhome/95/c/135723/FID_calculation/gland/data/stage1/dcgan_pac2/img
# sagan base
#python3 stage1/main_sagan_pac.py --dataset=gland --ngf=32 --mode=inference --gen_amount=100 --image_size=256 --nc=1 --model_path=/zhome/95/c/135723/Models/s1/Jul17_16-14-20/11000 --inf_out=/zhome/95/c/135723/FID_calculation/gland/data/stage1/sagan/img/
# sagan pac5
#python3 stage1/main_sagan_pac.py --dataset=gland --ngf=32 --mode=inference --gen_amount=100 --image_size=256 --nc=1 --model_path=/zhome/95/c/135723/Models/s1/Jul19_15-45-49/6000 --inf_out=/zhome/95/c/135723/FID_calculation/gland/data/stage1/sagan_pac5/img/
# sagan pac2
#python3 stage1/main_sagan_pac.py --dataset=gland --ngf=32 --mode=inference --gen_amount=100 --image_size=256 --nc=1 --model_path=/zhome/95/c/135723/Models/s1/Jul20_09-27-57/12000 --inf_out=/zhome/95/c/135723/FID_calculation/gland/data/stage1/sagan_pac2/img/
# sagan_hb
#python3 stage1/main_sagan_pac.py --dataset=gland --ngf=32 --mode=inference --gen_amount=100 --image_size=256 --nc=1 --model_path=/zhome/95/c/135723/Models/s1/Jul19_16-27-03/12000 --inf_out=/zhome/95/c/135723/FID_calculation/gland/data/stage1/sagan_hb/img/
# sagan_pac2_hb
#python3 stage1/main_sagan_pac.py --dataset=gland --ngf=32 --mode=inference --gen_amount=100 --image_size=256 --nc=1 --model_path=/zhome/95/c/135723/Models/s1/Jul21_09-53-13/12000 --inf_out=/zhome/95/c/135723/FID_calculation/gland/data/stage1/sagan_pac2_hb/img/
# sagan_pac2_hb_long
#python3 stage1/main_sagan_pac.py --dataset=gland --ngf=32 --mode=inference --gen_amount=100 --image_size=256 --nc=1 --model_path=/zhome/95/c/135723/Models/s1/Jul23_09-49-13/22000 --inf_out=/zhome/95/c/135723/FID_calculation/gland/data/stage1/sagan_pac2_hb_long/img/
# sagan_pac5_hb_long
#python3 stage1/main_sagan_pac.py --dataset=gland --ngf=32 --mode=inference --gen_amount=100 --image_size=256 --nc=1 --model_path=/zhome/95/c/135723/Models/s1/Jul25_19-51-11/22000 --inf_out=/zhome/95/c/135723/FID_calculation/gland/data/stage1/sagan_pac5_hb_long/img/


#drive
python3 stage1/main_sagan_pac.py --dataset=drive --ngf=32 --mode=inference --gen_amount=100 --image_size=256 --nc=1 --model_path=/zhome/95/c/135723/Models/s1/Jul28_11-47-55/12000 --inf_out=/zhome/95/c/135723/FID_calculation/drive/data/stage1/sagan_pac5_hb/img/ --extension=jpeg
python3 stage1/main_sagan_pac.py --dataset=drive --ngf=32 --mode=inference --gen_amount=100 --image_size=256 --nc=1 --model_path=/zhome/95/c/135723/Models/s1/Jul28_11-47-55/12000 --inf_out=/zhome/95/c/135723/Datasets/DRIVE_HD/fake/test_A --extension=jpeg

