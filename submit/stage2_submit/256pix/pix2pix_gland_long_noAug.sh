#!/bin/sh
### General options
### –- specify queue --
#BSUB -q gpuv100
### -- set the job Name --
#BSUB -J pix_512_save_noAug
### -- ask for number of cores (default: 1) --
#BSUB -n 1
### -- Select the resources: 1 gpu in exclusive process mode --
#BSUB -gpu "num=1:mode=exclusive_process"
### -- set walltime limit: hh:mm --  maximum 24 hours for GPU-queues right now
#BSUB -W 23:59
# request 5GB of system-memory
#BSUB -R "rusage[mem=128GB]"
# select the amount of GPU memory needed
#BSUB -R "select[gpu32gb]"
### -- set the email address --
# please uncomment the following line and put in your e-mail address,
# if you want to receive e-mail notifications on a non-default address
##BSUB -u s182902@student.dtu.dk
### -- send notification at start --
#BSUB -B
### -- send notification at completion--
#BSUB -N
### -- Specify the output and error file. %J is the job-id --
### -- -o and -e mean append, -oo and -eo mean overwrite --
#BSUB -o logs_2/gpu-%J.out
#BSUB -e logs_2/gpu_%J.err
# -- end of LSF options --

nvidia-smi
# Load the cuda module
module load cuda/10.2
module load python3/3.7.5

/appl/cuda/10.2/samples/NVIDIA_CUDA-10.2_Samples/bin/x86_64/linux/release/deviceQuery

# python3 scripts/stage2/main.py --stage=2 --batch_size=1 --image_size=256 --dataset=gland --epochs=100 --model_save_step=10 --sample_step=10 --log_step=1 --lr=0.0002 --normalize=1 --center_mean=0 --split_data=1.0 --data_aug=False
python3 scripts/stage2/main.py --stage=2 --batch_size=1 --image_size=256 --dataset=gland --epochs=100 --model_save_step=10 --sample_step=10 --log_step=1 --lr=0.0002 --normalize=1 --center_mean=0 --split_data=1.0 --data_aug=False --gen=ResUnet_a --pool_size=200




