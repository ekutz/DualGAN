from __future__ import print_function
# %matplotlib inline
import random
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
import numpy as np
import matplotlib.pyplot as plt
import socket
from stage1.config import *
from datetime import datetime
from torch.utils.tensorboard import SummaryWriter
from stage1.msg_pac_sagan.train import SaganTraining, SaganInference
from fid.fid import FID, calculate_frechet_distance
from collections import OrderedDict
from hyperopt import fmin, tpe, Trials, hp
from stage1.data_utils.utils import ObjectView
import stage1.metric

DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro".lower()) else False

if DEBUG:
    DATAROOT = "/Users/eike/Datasets/img_align_celebb"
    DATAROOT_MESSIDOR = "/Users/eike/Datasets/Messidor/Base11_anno/"
    DATAROOT_DIR_DRIVE = '/Users/eike/Datasets/DRIVE_masks/training'
    DATAROOT_DIR_GLAND = '/Users/eike/Datasets/Warwick_QU_Dataset/train/'
else:
    DATAROOT = "~/Datasets/celeba"
    DATAROOT_MESSIDOR = "~/Datasets/Messidor_mod/train/"
    DATAROOT_DIR_DRIVE = "/zhome/95/c/135723/Datasets/DRIVE_masks/training/"
    DATAROOT_DIR_GLAND = "/zhome/95/c/135723/Datasets/Warwick_QU_Dataset_masks/train/"

DATAROOT_MNIST = "data/mnist"


def objective(args):
    """Objective function for Bayesian optimization
    """
    global dataloader
    args_temp = ObjectView(args)
    args_temp.pac_factor = args_temp.pac_factor.item()
    dataloader = torch.utils.data.DataLoader(args_temp.dataset, batch_size=args_temp.batch_size.item(), shuffle=True,
                                             num_workers=args_temp.workers)

    # create model based on given values
    trainer = SaganTraining(args_temp, dataloader)
    trainer.train()
    if not DEBUG:
        s = metric.compute_score_raw(args_temp.dataset, args_temp.image_size, args_temp.dataroot,
                                     args_temp.sampleSize, args_temp.batch_size.item(),
                                     args_temp.outf + '/real/', args_temp.outf + '/fake/',
                                     trainer.netG, args_temp.nz, conv_model='inception_v3',
                                     workers=int(args_temp.workers), epoch=0,
                                     sagan=True)
        # m2, s2 = fid.calculate_activation_statistics_numpy(fake_batch)
        # return calculate_frechet_distance(fid.m1, fid.s1, m2, s2)
        return -1 * s[28] + -1 * s[29] + s[30]
    else:
        return random.random()


if __name__ == '__main__':
    now = datetime.now()
    args = get_main_values()
    args.now = now

    # Set random seed for reproducibility
    if args.mode == 'train' or args.mode == 'optimize':
        manualSeed = 999
    else:
        manualSeed = random.randint(1, 10000)  # use if you want new results
    print("Random Seed: ", manualSeed)
    random.seed(manualSeed)
    np.random.seed(manualSeed)
    torch.manual_seed(manualSeed)

    # We can use an image folder dataset the way we have it setup.
    # Create the dataset
    if args.mode == 'train' or args.mode == 'optimize':
        # writer = SummaryWriter(flush_secs=1, log_dir='runs_stage1_pac/' + now.strftime("%b%d_%H-%M-%S") + '/')

        dataset = None
        if args.dataset == 'celeba':
            args.dataroot = DATAROOT
            dataset = dset.ImageFolder(root=args.dataroot,
                                       transform=transforms.Compose([
                                           transforms.Resize(args.image_size),
                                           transforms.CenterCrop(args.image_size),
                                           transforms.ToTensor(),
                                           transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                                       ]))
            if DEBUG:
                fid = None
            else:
                fid = FID(path=DATAROOT)
        elif args.dataset == 'messidor':
            args.dataroot = DATAROOT_MESSIDOR
            dataset = dset.ImageFolder(root=args.dataroot,
                                       transform=transforms.Compose([
                                           transforms.Grayscale(),
                                           transforms.Resize(args.image_size),
                                           transforms.CenterCrop(args.image_size),
                                           transforms.ToTensor(),
                                           # lambda x: x > 0,
                                           # lambda x: x.float(),
                                           transforms.Normalize([0.5], [0.5]),
                                       ]))
            args.nc = 1
            # if DEBUG:
            #     fid = None
            # else:
            #     fid = FID(path='/zhome/95/c/135723/Datasets/Messidor_mod/train/train_512')
        elif args.dataset == 'drive':
            args.dataroot = DATAROOT_DIR_DRIVE
            dataset = dset.ImageFolder(root=args.dataroot,

                                       transform=transforms.Compose([
                                           transforms.Grayscale(),
                                           transforms.Resize(args.image_size),
                                           transforms.CenterCrop(args.image_size),
                                           transforms.ToTensor(),
                                           # lambda x: x > 0,
                                           # lambda x: x.float(),
                                           transforms.Normalize([0.5], [0.5]),
                                       ]))
            args.nc = 1
            dataset.imgs = dataset.imgs[:15]
            dataset.targets = dataset.targets[:15]
            dataset.samples = dataset.samples[:15]
            # if DEBUG:
            #     fid = None
            # else:
            #     fid = FID(path='/zhome/95/c/135723/Datasets/DRIVE/train/1st_manual')
        elif args.dataset == 'gland':
            args.dataroot = DATAROOT_DIR_GLAND
            dataset = dset.ImageFolder(root=args.dataroot,

                                       transform=transforms.Compose([
                                           transforms.Grayscale(),
                                           transforms.Resize(args.image_size),
                                           transforms.CenterCrop(args.image_size),
                                           transforms.ToTensor(),
                                           lambda x: x > 0,
                                           lambda x: x.float(),
                                           transforms.Normalize([0.5], [0.5]),
                                       ]))
            args.nc = 1
            # if DEBUG:
            #     fid = None
            # else:
            #     fid = FID(path='/zhome/95/c/135723/Datasets/DRIVE/train/1st_manual')
        elif args.dataset == 'mnist':
            args.dataroot = DATAROOT_MNIST

            dataset = dset.MNIST(root=args.dataroot, train=True, download=True,
                                 transform=transforms.Compose([
                                     transforms.Resize(args.image_size),
                                     transforms.CenterCrop(args.image_size),
                                     transforms.ToTensor(),
                                     transforms.Normalize([0.5], [0.5]),
                                 ]))
            args.nc = 1
            # if DEBUG:
            fid = None
            # else:
            #     fid = FID(path=DATAROOT_MNIST)
        # Create the dataloader
        args.dataset = dataset
        if args.mode != 'optimize':
            dataloader = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=True,
                                                     num_workers=args.workers)
    # Decide which device we want to run on
    device = torch.device("cuda:0" if (torch.cuda.is_available() and args.ngpu > 0) else "cpu")
    if args.mode == 'train':

        # Plot some training images
        real_batch = next(iter(dataloader))
        plt.figure(figsize=(8, 8))
        plt.axis("off")
        plt.title("Training Images")
        mask = vutils.make_grid(real_batch[0].to(device)[:64], padding=2, normalize=True).cpu()

        masks = np.transpose(mask, (1, 2, 0))
        plt.imshow(masks)
        if DEBUG:
            plt.show()
    if args.mode == 'train':

        writer = SummaryWriter(flush_secs=1, log_dir=args.tb_dir + '/' + now.strftime("%b%d_%H-%M-%S") + '/')
        writer.add_images('training_images', mask.numpy()[np.newaxis], 0)
        trainer = SaganTraining(args, dataloader, writer)
        trainer.train()
    elif args.mode == 'optimize':
        SPACE = OrderedDict([
            ('mode', args.mode),
            ('epochs', args.epochs),
            ('image_size', args.image_size),
            ('batch_size', hp.randint('batch_size', 35) + 5),
            ('ndf', hp.choice('ndf', [16, 32, 64])),
            ('ngf', hp.choice('ngf', [32, 64])),
            ('lr', hp.loguniform('lr', np.log(0.00001), np.log(0.001))),
            ('pac_factor', hp.randint('pac_factor', 4) + 1),
            ('nz', args.nz),
            ('dataset', dataset),
            ('nc', args.nc),
            ('beta1', args.beta1),
            ('ngpu', args.ngpu),
            ('save_model_step', args.save_model_step),
            ('workers', args.workers),
            ('outf', args.outf),
            ('sampleSize', args.sampleSize),
            ('dataroot', args.dataroot),
            ('use_metric', False),
            ('adv_loss', args.adv_loss)
        ])
        trials = Trials()
        best = fmin(objective, SPACE, algo=tpe.suggest, max_evals=args.max_evals, trials=trials)
        with SummaryWriter(log_dir=args.tb_dir + args.now.strftime("%b%d_%H-%M-%S") + '/') as w:
            for i in range(len(trials.results)):
                hyperparams = {}
                for key in trials.vals.keys():
                    if isinstance(trials.vals[key][i], np.int64):
                        hyperparams[key] = int(trials.vals[key][i])
                    elif isinstance(trials.vals[key][i], np.float64):
                        hyperparams[key] = float(trials.vals[key][i])
                    else:
                        hyperparams[key] = trials.vals[key][i]
                w.add_hparams(hyperparams,
                              {'hparam/FID': trials.results[i]['loss']})
        print(best)

    elif args.mode == 'inference':
        inference = SaganInference(args)
        inference.inference()
    elif args.mode == 'interpolate_latent_space':
        inference = SaganInference(args)
        inference.interpolate_latent_space()
