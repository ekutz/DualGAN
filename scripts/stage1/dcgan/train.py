import torch
import torch.nn as nn
from stage1.dcgan.model import Generator, Discriminator
import torch.optim as optim
import torchvision.utils as vutils
import matplotlib.pyplot as plt
import socket
import numpy as np
from typing import Union, Optional, Sequence, Tuple, Text, BinaryIO
import math
from PIL import Image
import os

DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro-2".lower()) else False
MODEL_PATH = '../Models/'
if DEBUG:
    IMAGE_SAVE_PATH = "../../../Datasets/Messidor/fake_anno_256/"
else:
    IMAGE_SAVE_PATH = "../Datasets/Messidor/fake_anno_256/"


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)


def normalize(tensor, range: Optional[Tuple[int, int]] = None, scale_each: bool = False):
    tensor = tensor.clone()  # avoid modifying tensor in-place
    if range is not None:
        assert isinstance(range, tuple), \
            "range has to be a tuple (min, max) if specified. min and max are numbers"

    def norm_ip(img, min, max):
        img.clamp_(min=min, max=max)
        img.add_(-min).div_(max - min + 1e-5)

    def norm_range(t, range):
        if range is not None:
            norm_ip(t, range[0], range[1])
        else:
            norm_ip(t, float(t.min()), float(t.max()))

    if scale_each is True:
        for t in tensor:  # loop over mini-batch dimension
            norm_range(t, range)
    else:
        norm_range(tensor, range)
    return tensor


def save_image(image, path):
    im = Image.fromarray(image)
    im = im.convert("L")
    im.save(path + '.png', "PNG")


class DcganInference:
    def __init__(self, args):
        self.args = args

        self.device = torch.device("cuda:0" if (torch.cuda.is_available() and self.args.ngpu > 0) else "cpu")
        # init generator
        self.netG = Generator(self.args.ngpu, nz=self.args.nz, ngf=self.args.ngf, nc=self.args.nc).to(self.device)
        #modelpath = '{}/{}/epoch_{}.pt'.format(MODEL_PATH, args.model_date, args.model_epoch)

        if self.device.type == 'cpu':

            checkpoint = torch.load(self.args.model_path, map_location=lambda storage, location: storage)
        else:
            checkpoint = torch.load(self.args.model_path)

        # load generator
        self.netG.load_state_dict(checkpoint['netG_state_dict'])

    def inference(self):
        total_runs = int(math.ceil(self.args.gen_amount / self.args.batch_size))
        n = 0
        os.makedirs(IMAGE_SAVE_PATH, exist_ok=True)
        for i in range(total_runs):
            noise = torch.randn(self.args.batch_size, self.args.nz, 1, 1, device=self.device)

            fake = self.netG(noise).detach().cpu()

            fake = normalize(fake).numpy()
            for f in fake:
                if n >= self.args.gen_amount:
                    break
                image = f[0] * 255
                if DEBUG:
                    plt.imshow(image)
                    plt.show()
                fname = "fake" + str(n)
                save_image(image, os.path.join(IMAGE_SAVE_PATH, fname))
                n += 1


class DcganTraining:
    def __init__(self, args, dataloader, writer=None):
        print(
            "batch_size: " + str(args.batch_size) + " lr: " + str(args.lr) + " ngf: " + str(args.ngf) + " ndf: " + str(
                args.ndf))
        self.args = args
        self.dataloader = dataloader
        self.device = torch.device("cuda:0" if (torch.cuda.is_available() and self.args.ngpu > 0) else "cpu")

        # Create the generator
        self.netG = Generator(self.args.ngpu, nz=self.args.nz, ngf=self.args.ngf, nc=self.args.nc).to(self.device)

        # Handle multi-gpu if desired
        if (self.device.type == 'cuda') and (self.args.ngpu > 1):
            self.netG = nn.DataParallel(self.netG, list(range(self.args.ngpu)))

        # Apply the weights_init function to randomly initialize all weights
        #  to mean=0, stdev=0.2.
        self.netG.apply(weights_init)

        # Print the model
        if self.args.mode == 'train':
            print(self.netG)

        # Create the Discriminator
        self.netD = Discriminator(self.args.ngpu, ndf=self.args.ndf, nc=self.args.nc).to(self.device)

        # Handle multi-gpu if desired
        if (self.device.type == 'cuda') and (self.args.ngpu > 1):
            self.netD = nn.DataParallel(self.netD, list(range(self.args.ngpu)))

        # Apply the weights_init function to randomly initialize all weights
        #  to mean=0, stdev=0.2.
        self.netD.apply(weights_init)

        # Print the model
        if self.args.mode == 'train':
            print(self.netD)
        # %%

        # Initialize BCELoss function
        self.criterion = nn.BCELoss()

        # Create batch of latent vectors that we will use to visualize
        #  the progression of the generator
        self.fixed_noise = torch.randn(64, self.args.nz, 1, 1, device=self.device)
        self.fixed_noise_fid = torch.randn(100, self.args.nz, 1, 1, device=self.device)
        # Setup Adam optimizers for both G and D
        self.optimizerD = optim.Adam(self.netD.parameters(), lr=self.args.lr, betas=(self.args.beta1, 0.999))
        self.optimizerG = optim.Adam(self.netG.parameters(), lr=self.args.lr, betas=(self.args.beta1, 0.999))

        # Logging
        self.writer = writer

    def train(self):
        # Establish convention for real and fake labels during training
        real_label = 1
        fake_label = 0
        # Training Loop

        # Lists to keep track of progress
        G_losses = []
        D_losses = []
        iters = 0
        if self.args.mode == 'train':
            print("Starting Training Loop...")

        for epoch in range(self.args.epochs):
            # For each batch in the dataloader
            for i, data in enumerate(self.dataloader, 0):

                ############################
                # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
                ###########################
                ## Train with all-real batch
                self.netD.zero_grad()
                # Format batch
                real_cpu = data[0].to(self.device)
                b_size = real_cpu.size(0)
                label = torch.full((b_size,), real_label, device=self.device)
                # Forward pass real batch through D
                output = self.netD(real_cpu).view(-1)
                # Calculate loss on all-real batch
                errD_real = self.criterion(output, label)
                # Calculate gradients for D in backward pass
                errD_real.backward()
                D_x = output.mean().item()

                ## Train with all-fake batch
                # Generate batch of latent vectors
                noise = torch.randn(b_size, self.args.nz, 1, 1, device=self.device)
                # Generate fake image batch with G
                fake = self.netG(noise)
                label.fill_(fake_label)
                # Classify all fake batch with D
                output = self.netD(fake.detach()).view(-1)
                # Calculate D's loss on the all-fake batch
                errD_fake = self.criterion(output, label)
                # Calculate the gradients for this batch
                errD_fake.backward()
                D_G_z1 = output.mean().item()
                # Add the gradients from the all-real and all-fake batches
                errD = errD_real + errD_fake
                # Update D
                self.optimizerD.step()

                ############################
                # (2) Update G network: maximize log(D(G(z)))
                ###########################
                self.netG.zero_grad()
                label.fill_(real_label)  # fake labels are real for generator cost
                # Since we just updated D, perform another forward pass of all-fake batch through D
                output = self.netD(fake).view(-1)
                # Calculate G's loss based on this output
                errG = self.criterion(output, label)
                # Calculate gradients for G
                errG.backward()
                D_G_z2 = output.mean().item()
                # Update G
                self.optimizerG.step()

                if self.args.mode == 'train':
                    # Output training stats
                    if i % 50 == 0:
                        print('[%d/%d][%d/%d]\tLoss_D: %.4f\tLoss_G: %.4f\tD(x): %.4f\tD(G(z)): %.4f / %.4f'
                              % (epoch, self.args.epochs, i, len(self.dataloader),
                                 errD.item(), errG.item(), D_x, D_G_z1, D_G_z2))

                    # Save Losses for plotting later
                    G_losses.append(errG.item())
                    D_losses.append(errD.item())
                    if self.writer:
                        self.writer.add_scalar('Loss/errG', errG.item(), iters)
                        self.writer.add_scalar('Loss/errD', errD.item(), iters)

                # Check how the generator is doing by saving G's output on fixed_noise
                if self.writer:
                    if (iters % 500 == 0) or ((epoch == self.args.epochs - 1) and (i == len(self.dataloader) - 1)):
                        with torch.no_grad():
                            fake = self.netG(self.fixed_noise).detach().cpu()
                            img = vutils.make_grid(fake, padding=2, normalize=True)
                            # Plot the fake images from the last epoch
                            self.writer.add_images('fake_images', img.numpy()[np.newaxis], iters)
                        if DEBUG:
                            plt.figure(figsize=(15, 15))
                            plt.axis("off")
                            plt.title("Fake Images")
                            fake_images = np.transpose(img, (1, 2, 0))
                            plt.imshow(fake_images)
                            plt.show()
                iters += 1
                if DEBUG:
                    print(iters)
            if self.args.save_model_step > 0:
                if epoch % self.args.save_model_step == 0 and epoch > 0:
                    self.save_models(epoch=epoch)
        if self.args.mode == 'optimize':
            fake = self.netG(self.fixed_noise_fid).detach().cpu()
            return normalize(fake).numpy()

    def save_models(self, epoch):
        # save_path = '{}/{}/epoch_{}.pt'.format(MODEL_PATH, self.args.now, epoch)
        save_path = os.path.join(MODEL_PATH, self.args.now.strftime("%b%d_%H-%M-%S"))
        # if not os.path.exists(save_path):
        os.makedirs(save_path, exist_ok=True)
        save_path = os.path.join(save_path, str(epoch))
        torch.save({
            'epoch': epoch,
            'netG_state_dict': self.netG.state_dict(),
            # 'netD_state_dict': self.netD.state_dict(),
            # 'optimizerD_state_dict': self.optimizerD.state_dict(),
            # 'optimizerG_state_dict': self.optimizerG.state_dict(),
        }, save_path)
