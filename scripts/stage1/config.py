import argparse


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def get_main_values():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset', type=str, default='gland',
                        choices=['celeba', 'mnist', 'messidor', 'drive', 'gland'])
    parser.add_argument('--epochs', type=int, default=5)  # Number of training epochs
    parser.add_argument('--lr', type=float, default=0.0002)  # Learning rate for optimizers
    parser.add_argument('--ngf', type=int, default=64)  # Size of feature maps in generator
    parser.add_argument('--ndf', type=int, default=64)  # Size of feature maps in discriminator
    parser.add_argument('--nz', type=int, default=100)  # Size of z latent vector (i.e. size of generator input)
    parser.add_argument('--beta1', type=float, default=0.5)  # Beta1 hyperparam for Adam optimizers
    parser.add_argument('--ngpu', type=int, default=1)  # Number of GPUs available. Use 0 for CPU mode.
    parser.add_argument('--nc', type=int, default=3)  # Number of channels in the training images. 3for rgb
    parser.add_argument('--save_model_step', type=int, default=0)  # interval for saving the dis/gen models, 0=not
    parser.add_argument('--adv_loss', type=str, default='wgan-gp', choices=['hinge', 'wgan-gp'])
    parser.add_argument('--dis_net', type=str, default='Conv', choices=['Conv', 'Unet'])
    parser.add_argument('--image_size', type=int, default=512)  # Spatial size of training images. All images will be
    parser.add_argument('--tb_dir', type=str, default='runs_stage_1')
    # resized to this size using a transformer.
    parser.add_argument('--batch_size', type=int, default=16)  # Batch size during training
    parser.add_argument('--workers', type=int, default=2)  # Number of workers for dataloader
    parser.add_argument('--mode', type=str, default='train', choices=['train', 'optimize', 'inference',
                                                                      'interpolate_latent_space', 'tsne'])
    parser.add_argument('--max_evals', type=int, default='2')
    parser.add_argument('--add_noise', type=str2bool, default=False)
    # inference
    parser.add_argument('--gen_amount', type=int, default=1)  # amount of to be generated images in inference
    parser.add_argument('--model_path', type=str)
    parser.add_argument('--inf_out', type=str, default=None)
    parser.add_argument('--extension', type=str, default='bmp')
    # pac values
    parser.add_argument('--pac_factor', type=int, default=5)  # amount of samples in on sample pac

    # metric
    parser.add_argument('--use_metric', type=str2bool, default=False)
    parser.add_argument('--sampleSize', type=int, default=2000)
    parser.add_argument('--outf', default='results', help='folder to output images and model checkpoints')

    # history pooling
    parser.add_argument('--use_pool', type=str2bool, default=False)
    parser.add_argument('--pool_size', type=int, default=10)

    # tsne
    parser.add_argument('--fake_root', type=str)
    parser.add_argument('--tsne_plot', type=str2bool, default=False)

    return parser.parse_args()
