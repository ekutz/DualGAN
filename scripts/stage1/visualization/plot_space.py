import matplotlib.pyplot as plt
import numpy as np
from matplotlib import offsetbox
from skimage.transform import resize


def embedding_plot(x_0_pca, x_1_pca, x_0, x_1, title, args):
    x_min, x_max = np.min([np.min(x_0_pca, axis=0), np.min(x_1_pca, axis=0)], axis=0), np.max(
        [np.max(x_0_pca, axis=0), np.max(x_1_pca, axis=0)], axis=0)
    x_0_pca = (x_0_pca - x_min) / (x_max - x_min)
    x_1_pca = (x_1_pca - x_min) / (x_max - x_min)

    plt.figure(figsize=(20, 20))
    ax = plt.subplot(aspect='equal')
    sc0 = ax.scatter(x_0_pca[:, 0], x_0_pca[:, 1], lw=0, s=40, c='orange', label='fake', alpha=0)
    sc1 = ax.scatter(x_1_pca[:, 0], x_1_pca[:, 1], lw=0, s=40, c='crimson', label='real', alpha=0)

    shown_images = np.array([[1., 1.]])
    size = 50
    alpha = 0.5
    for i in range(x_0_pca.shape[0]):
        # if np.min(np.sum((X_0_pca[i] - shown_images) ** 2, axis=1)) < 1e-2:
        #     continue
        shown_images = np.r_[shown_images, [x_0_pca[i]]]
        ax.add_artist(offsetbox.AnnotationBbox(
            offsetbox.OffsetImage(
                resize(x_0[i].reshape(args.image_size, args.image_size), (size, size), mode='constant'),
                cmap=plt.cm.gray, alpha=alpha), x_0_pca[i],
            bboxprops=dict(edgecolor='orange', alpha=alpha, linewidth=4.0)))
    for i in range(x_1_pca.shape[0]):
        # if np.min(np.sum((X_1_pca[i] - shown_images) ** 2, axis=1)) < 1e-2:
        #     continue
        shown_images = np.r_[shown_images, [x_1_pca[i]]]
        ax.add_artist(offsetbox.AnnotationBbox(
            offsetbox.OffsetImage(
                resize(x_1[i].reshape(args.image_size, args.image_size), (size, size), mode='constant'),
                cmap=plt.cm.gray, alpha=alpha), x_1_pca[i],
            bboxprops=dict(edgecolor='crimson', alpha=alpha, linewidth=4.0)))

    plt.xticks([]), plt.yticks([])
    leg = plt.legend(prop={'size': 12})
    for lh in leg.legendHandles:
        lh.set_alpha(1)
    plt.title(title)
    plt.tight_layout()


def embedding_plot_single(x_pca, x, y, title, args):
    x_min, x_max = np.min(x_pca, axis=0), np.max(x_pca, axis=0)
    x_pca = (x_pca - x_min) / (x_max - x_min)

    plt.figure(figsize=(20, 20))
    ax = plt.subplot(aspect='equal')
    sc = ax.scatter(x_pca[:, 0], x_pca[:, 1], lw=0, s=40, c=['orange' if y_ == 0 else 'crimson' for y_ in y], alpha=0)

    size = 50
    alpha = 0.5
    shown_images = np.array([[1., 1.]])
    for i in range(x_pca.shape[0]):
        # if np.min(np.sum((x_pca[i] - shown_images) ** 2, axis=1)) < 1e-2: continue
        shown_images = np.r_[shown_images, [x_pca[i]]]
        ax.add_artist(offsetbox.AnnotationBbox(offsetbox.OffsetImage(
            resize(x[i].reshape(args.image_size, args.image_size), (size, size), mode='constant'),
            cmap=plt.cm.gray, alpha=alpha, label=y[i]), x_pca[i],
            bboxprops=dict(edgecolor='#950000' if y[i] == 0 else '#70AD47', alpha=alpha, linewidth=4.0,
                           label='synth' if y[i] == 0 else 'real')))

    plt.xticks([]), plt.yticks([])
    leg = plt.legend(['synth', 'real'], scatterpoints=1, prop={'size': 30}, markerscale=3)

    for lh in leg.legendHandles:
        lh.set_alpha(1)
    # plt.title(title)
    plt.tight_layout()


def embedding_plot_single2(x_pca, x, y, title, args):
    x_min, x_max = np.min(x_pca, axis=0), np.max(x_pca, axis=0)
    x_pca = (x_pca - x_min) / (x_max - x_min)

    plt.figure(figsize=(20, 20))
    ax = plt.subplot(aspect='equal')
    sc = ax.scatter(x_pca[:, 0], x_pca[:, 1], lw=0, s=40, c=['orange' if y_ == 0 else 'crimson' for y_ in y], alpha=0)

    size = 50
    alpha = 0.5
    shown_images = np.array([[1., 1.]])
    for i in range(x_pca.shape[0]):
        # if np.min(np.sum((x_pca[i] - shown_images) ** 2, axis=1)) < 1e-2: continue
        shown_images = np.r_[shown_images, [x_pca[i]]]
        if y[i] == 0:
            l1 = ax.add_artist(offsetbox.AnnotationBbox(offsetbox.OffsetImage(
                resize(x[i].reshape(args.image_size, args.image_size), (size, size), mode='constant'),
                cmap=plt.cm.gray, alpha=alpha, label='synth' if y[i] == 0 else 'real'), x_pca[i],
                bboxprops=dict(edgecolor='#950000' if y[i] == 0 else '#70AD47', alpha=alpha, linewidth=4.0,
                               label='synth' if y[i] == 0 else 'real')))
        else:
            l2 = ax.add_artist(offsetbox.AnnotationBbox(offsetbox.OffsetImage(
                resize(x[i].reshape(args.image_size, args.image_size), (size, size), mode='constant'),
                cmap=plt.cm.gray, alpha=alpha, label='synth' if y[i] == 0 else 'real'), x_pca[i],
                bboxprops=dict(edgecolor='#950000' if y[i] == 0 else '#70AD47', alpha=alpha, linewidth=4.0,
                               label='synth' if y[i] == 0 else 'real')))

    plt.xticks([]), plt.yticks([])
    # leg = plt.legend([l1,l2])
    import matplotlib.patches as mpatches
    patch2 = mpatches.Patch(color='#950000', label='synth')
    patch = mpatches.Patch(color='#70AD47', label='real')
    handles, labels = ax.get_legend_handles_labels()

    handles.append(patch)
    handles.append(patch2)
    for lh in handles:
        lh.set_alpha(1)
    plt.legend(handles=handles, labels=['real', 'synth'], scatterpoints=1, prop={'size': 30}, markerscale=3)


    # plt.title(title)
    plt.tight_layout()
