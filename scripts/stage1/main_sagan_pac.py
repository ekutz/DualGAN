from __future__ import print_function
# %matplotlib inline
import random
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
import numpy as np
import matplotlib.pyplot as plt
import socket
from stage1.config import *
from datetime import datetime
from torch.utils.tensorboard import SummaryWriter
from stage1.pac_sagan.train import SaganTraining, SaganInference
from fid.fid import FID, calculate_frechet_distance
from collections import OrderedDict
from hyperopt import fmin, tpe, Trials, hp
from stage1.data_utils.utils import ObjectView
import stage1.metric
from functools import partial

DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro".lower()) else False

if DEBUG:
    DATAROOT = "/Users/eike/Datasets/img_align_celebb"
    DATAROOT_MESSIDOR = "/Users/eike/Datasets/Messidor/Base11_anno/"
    DATAROOT_DIR_DRIVE = '/Users/eike/Datasets/DRIVE_HD/train_A_stage1/'
    DATAROOT_DIR_GLAND = '/Users/eike/Datasets/Warwick_QU_Dataset/train/'
else:
    DATAROOT = "~/Datasets/celeba"
    DATAROOT_MESSIDOR = "~/Datasets/Messidor_mod/train/"
    DATAROOT_DIR_DRIVE = "/zhome/95/c/135723/Datasets/DRIVE_HD/train_A_stage1/"
    DATAROOT_DIR_GLAND = "/zhome/95/c/135723/Datasets/Warwick_QU_Dataset_masks/train/"

DATAROOT_MNIST = "data/mnist"


def objective(args):
    """Objective function for Bayesian optimization
    """
    global dataloader
    args_temp = ObjectView(args)
    args_temp.pac_factor = args_temp.pac_factor.item()
    dataloader = torch.utils.data.DataLoader(args_temp.dataset, batch_size=args_temp.batch_size.item(), shuffle=True,
                                             num_workers=args_temp.workers)

    # create model based on given values
    trainer = SaganTraining(args_temp, dataloader)
    trainer.train()
    if not DEBUG:
        s = metric.compute_score_raw(args_temp.dataset, args_temp.image_size, args_temp.dataroot,
                                     args_temp.sampleSize, args_temp.batch_size.item(),
                                     args_temp.outf + '/real/', args_temp.outf + '/fake/',
                                     trainer.netG, args_temp.nz, conv_model='inception_v3',
                                     workers=int(args_temp.workers), epoch=0,
                                     sagan=True)
        # m2, s2 = fid.calculate_activation_statistics_numpy(fake_batch)
        # return calculate_frechet_distance(fid.m1, fid.s1, m2, s2)
        return -1 * s[28] + -1 * s[29] + s[30]
    else:
        return random.random()


if __name__ == '__main__':
    now = datetime.now()
    args = get_main_values()
    args.now = now

    # Set random seed for reproducibility
    if args.mode == 'train' or args.mode == 'optimize':
        manualSeed = 999
    else:
        manualSeed = random.randint(1, 10000)  # use if you want new results
    print("Random Seed: ", manualSeed)
    random.seed(manualSeed)
    np.random.seed(manualSeed)
    torch.manual_seed(manualSeed)

    # We can use an image folder dataset the way we have it setup.
    # Create the dataset
    if args.mode == 'train' or args.mode == 'optimize':
        # writer = SummaryWriter(flush_secs=1, log_dir='runs_stage1_pac/' + now.strftime("%b%d_%H-%M-%S") + '/')

        dataset = None
        if args.dataset == 'celeba':
            args.dataroot = DATAROOT
            dataset = dset.ImageFolder(root=args.dataroot,
                                       transform=transforms.Compose([
                                           transforms.Resize(args.image_size),
                                           transforms.CenterCrop(args.image_size),
                                           transforms.ToTensor(),
                                           transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                                       ]))
            if DEBUG:
                fid = None
            else:
                fid = FID(path=DATAROOT)
        elif args.dataset == 'messidor':
            args.dataroot = DATAROOT_MESSIDOR
            dataset = dset.ImageFolder(root=args.dataroot,
                                       transform=transforms.Compose([
                                           transforms.Grayscale(),
                                           transforms.Resize(args.image_size),
                                           transforms.CenterCrop(args.image_size),
                                           transforms.ToTensor(),
                                           # lambda x: x > 0,
                                           # lambda x: x.float(),
                                           transforms.Normalize([0.5], [0.5]),
                                       ]))
            args.nc = 1
            # if DEBUG:
            #     fid = None
            # else:
            #     fid = FID(path='/zhome/95/c/135723/Datasets/Messidor_mod/train/train_512')
        elif args.dataset == 'drive':
            args.dataroot = DATAROOT_DIR_DRIVE
            dataset = dset.ImageFolder(root=args.dataroot,

                                       transform=transforms.Compose([
                                           transforms.Grayscale(),
                                           transforms.Resize(args.image_size),
                                           transforms.CenterCrop(args.image_size),
                                           transforms.ToTensor(),
                                           # lambda x: x > 0,
                                           # lambda x: x.float(),
                                           transforms.Normalize([0.5], [0.5]),
                                       ]))
            args.nc = 1
            # dataset.imgs = dataset.imgs[:15]
            # dataset.targets = dataset.targets[:15]
            # dataset.samples = dataset.samples[:15]
            # # if DEBUG:
            # #     fid = None
            # # else:
            # #     fid = FID(path='/zhome/95/c/135723/Datasets/DRIVE/train/1st_manual')
        elif args.dataset == 'gland':
            args.dataroot = DATAROOT_DIR_GLAND
            dataset = dset.ImageFolder(root=args.dataroot,
                                       transform=transforms.Compose([
                                           transforms.Grayscale(),
                                           transforms.Resize(args.image_size),
                                           transforms.CenterCrop(args.image_size),
                                           transforms.ToTensor(),
                                           lambda x: x > 0,
                                           lambda x: x.float(),
                                           # transforms.Normalize([mean], [std]),
                                           transforms.Normalize([0.5], [0.5]),
                                       ]))

            args.nc = 1
            # if DEBUG:
            #     fid = None
            # else:
            #     fid = FID(path='/zhome/95/c/135723/Datasets/DRIVE/train/1st_manual')
        elif args.dataset == 'mnist':
            args.dataroot = DATAROOT_MNIST

            dataset = dset.MNIST(root=args.dataroot, train=True, download=True,
                                 transform=transforms.Compose([
                                     transforms.Resize(args.image_size),
                                     transforms.CenterCrop(args.image_size),
                                     transforms.ToTensor(),
                                     transforms.Normalize([0.5], [0.5]),
                                 ]))
            args.nc = 1
            # if DEBUG:
            fid = None
            # else:
            #     fid = FID(path=DATAROOT_MNIST)
        # Create the dataloader
        args.dataset = dataset
        if args.mode != 'optimize':
            dataloader = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=True,
                                                     num_workers=args.workers)
    # Decide which device we want to run on
    device = torch.device("cuda:0" if (torch.cuda.is_available() and args.ngpu > 0) else "cpu")
    if args.mode == 'train':

        # Plot some training images
        real_batch = next(iter(dataloader))
        plt.figure(figsize=(8, 8))
        plt.axis("off")
        plt.title("Training Images")
        mask = vutils.make_grid(real_batch[0].to(device)[:64], padding=2, normalize=True).cpu()

        masks = np.transpose(mask, (1, 2, 0))
        plt.imshow(masks)
        if DEBUG:
            plt.show()
    if args.mode == 'train':

        writer = SummaryWriter(flush_secs=1, log_dir=args.tb_dir + '/' + now.strftime("%b%d_%H-%M-%S") + '/')
        writer.add_images('training_images', mask.numpy()[np.newaxis], 0)
        trainer = SaganTraining(args, dataloader, writer)
        trainer.train()
    elif args.mode == 'optimize':
        SPACE = OrderedDict([
            ('mode', args.mode),
            ('epochs', args.epochs),
            ('image_size', args.image_size),
            ('batch_size', hp.randint('batch_size', 35) + 5),
            ('ndf', hp.choice('ndf', [16, 32, 64])),
            ('ngf', hp.choice('ngf', [32, 64])),
            ('lr', hp.loguniform('lr', np.log(0.00001), np.log(0.001))),
            ('pac_factor', hp.randint('pac_factor', 4) + 1),
            ('nz', args.nz),
            ('dataset', dataset),
            ('nc', args.nc),
            ('beta1', args.beta1),
            ('ngpu', args.ngpu),
            ('save_model_step', args.save_model_step),
            ('workers', args.workers),
            ('outf', args.outf),
            ('sampleSize', args.sampleSize),
            ('dataroot', args.dataroot),
            ('use_metric', False),
            ('adv_loss', args.adv_loss),
            ('dis_net', args.dis_net),
            ('use_pool', args.use_pool),
            ('pool_size', args.pool_size),
            ('add_noise', args.add_noise)
        ])
        trials = Trials()
        best = fmin(objective, SPACE, algo=partial(tpe.suggest, n_startup_jobs=int(args.max_evals // 2)),
                    max_evals=args.max_evals, trials=trials)
        with SummaryWriter(log_dir=args.tb_dir + args.now.strftime("%b%d_%H-%M-%S") + '/') as w:
            for i in range(len(trials.results)):
                hyperparams = {}
                for key in trials.vals.keys():
                    if isinstance(trials.vals[key][i], np.int64):
                        hyperparams[key] = int(trials.vals[key][i])
                    elif isinstance(trials.vals[key][i], np.float64):
                        hyperparams[key] = float(trials.vals[key][i])
                    else:
                        hyperparams[key] = trials.vals[key][i]
                w.add_hparams(hyperparams,
                              {'hparam/FID': trials.results[i]['loss']})
        print(best)

    elif args.mode == 'inference':
        inference = SaganInference(args)
        inference.inference()
    elif args.mode == 'interpolate_latent_space':
        inference = SaganInference(args)
        inference.interpolate_latent_space()
    elif args.mode == 'tsne':
        from sklearn.manifold import TSNE
        from sklearn import manifold, datasets, decomposition, discriminant_analysis
        from visualization import plot_space
        import os

        args.nc = 1
        args.tsne_plot = True
        SAVE_PATH = '/Users/eike/DTU/Evaluation/DisAnalysis/'
        inference = SaganInference(args)
        args.dataroot = '/Users/eike/DTU/Evaluation/DisAnalysis/GLAND'
        dataset = dset.ImageFolder(root=args.dataroot,
                                   transform=transforms.Compose([
                                       transforms.Grayscale(),
                                       transforms.Resize(args.image_size),
                                       transforms.CenterCrop(args.image_size),
                                       transforms.ToTensor(),
                                       lambda x: x > 0,
                                       lambda x: x.float(),
                                       # transforms.Normalize([mean], [std]),
                                       transforms.Normalize([0.5], [0.5]),
                                   ]))
        dataloader_real = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=True,
                                                      num_workers=args.workers)
        dataset = dset.ImageFolder(root=args.fake_root,
                                   transform=transforms.Compose([
                                       transforms.Grayscale(),
                                       transforms.Resize(args.image_size),
                                       transforms.CenterCrop(args.image_size),
                                       transforms.ToTensor(),
                                       lambda x: x > 0,
                                       lambda x: x.float(),
                                       # transforms.Normalize([mean], [std]),
                                       transforms.Normalize([0.5], [0.5]),
                                   ]))
        dataloader_fake = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=True,
                                                      num_workers=args.workers)

        X_1 = np.concatenate([y.flatten(start_dim=1).numpy() for x, _ in dataloader_real for y in x], axis=0)
        X_0 = np.concatenate([y.flatten(start_dim=1).numpy() for x, _ in dataloader_fake for y in x], axis=0)
        print(X_1.shape, X_0.shape)
        # for x, _ in dataloader_real:
        #     test = inference.discriminator_raw_output(x)
        #     print(test.shape)
        #     print(test.view(x.shape[0],-1).shape)
        X_1_latent = np.concatenate(
            [inference.discriminator_raw_output(x).detach().view(x.shape[0], -1).numpy() for x, _ in dataloader_real], axis=0)
        X_0_latent = np.concatenate(
            [inference.discriminator_raw_output(x).detach().view(x.shape[0], -1).numpy() for x, _ in dataloader_fake], axis=0)
        # X_0_latent = [y for x, _ in dataloader_fake for y in
        #               inference.discriminator_raw_output(x).detach().view(x.shape[0], -1).numpy()]
        print(X_0_latent.shape, X_1_latent.shape)
        X = np.concatenate((X_0_latent, X_1_latent), axis=0)
        y = np.concatenate((np.array(len(X_1_latent) * [1]), np.array(len(X_0_latent) * [0])))

        # pca = decomposition.PCA(n_components=2)
        from time import time
        from sklearn.cluster import KMeans
        from sklearn import metrics
        from sklearn.decomposition import PCA
        from sklearn.preprocessing import scale
        print(X[0].dtype)
        X = np.float64(X)
        print(X[0].dtype)
        data = (X,y)
        data = scale(X)
        labels = y
        sample_size = 185


        def bench_k_means(estimator, name, data):
            t0 = time()
            estimator.fit(data)
            print('%-9s\t%.2fs\t%i\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f'
                  % (name, (time() - t0), estimator.inertia_,
                     metrics.homogeneity_score(labels, estimator.labels_),
                     metrics.completeness_score(labels, estimator.labels_),
                     metrics.v_measure_score(labels, estimator.labels_),
                     metrics.adjusted_rand_score(labels, estimator.labels_),
                     metrics.adjusted_mutual_info_score(labels, estimator.labels_),
                     metrics.silhouette_score(data, estimator.labels_,
                                              metric='euclidean',
                                              sample_size=sample_size)))


        bench_k_means(KMeans(init='k-means++', n_clusters=2, n_init=10),
                      name="k-means++", data=data)

        bench_k_means(KMeans(init='random', n_clusters=2, n_init=10),
                      name="random", data=data)
        # X_1_pca = pca.fit_transform(X_1_latent)
        # X_0_pca = pca.fit_transform(X_0_latent)
        # plot_space.embedding_plot(X_0_pca, X_1_pca, X_0, X_1, "GLAND PCA-latentspace", args)
        # plt.savefig(os.path.join(SAVE_PATH, args.dataset + '_PCA_latentspace.png'), transparent=True)
        # plt.show()
        for perp in range(45, 50, 5):
            tsne = TSNE(n_components=2, verbose=0, perplexity=perp, n_iter=500, method='exact')
            tsne_results = tsne.fit_transform(X)



            plot_space.embedding_plot_single2(tsne_results, np.concatenate((X_0, X_1), axis=0), y, 't-sne', args)
            # plt.savefig(os.path.join(SAVE_PATH, args.dataset + '_TSNE-discriminator_space'+str(perp)+'.png'), transparent=True)

        n_samples, n_features = data.shape
        n_digits = len(np.unique(y))
        labels = y

        sample_size = 300

        print("n_digits: %d, \t n_samples %d, \t n_features %d"
              % (n_digits, n_samples, n_features))

        print(82 * '_')
        print('init\t\ttime\tinertia\thomo\tcompl\tv-meas\tARI\tAMI\tsilhouette')

        bench_k_means(KMeans(init='k-means++', n_clusters=n_digits, n_init=10),
                      name="k-means++", data=data)

        bench_k_means(KMeans(init='random', n_clusters=n_digits, n_init=10),
                      name="random", data=data)

        # in this case the seeding of the centers is deterministic, hence we run the
        # kmeans algorithm only once with n_init=1
        pca = PCA(n_components=n_digits).fit(data)
        bench_k_means(KMeans(init=pca.components_, n_clusters=n_digits, n_init=1),
                      name="PCA-based",
                      data=data)
        print(82 * '_')


        # #############################################################################
        # Visualize the results on PCA-reduced data

        reduced_data = PCA(n_components=2).fit_transform(data)
        kmeans = KMeans(init='k-means++', n_clusters=n_digits, n_init=10)
        kmeans.fit(reduced_data)

        # Step size of the mesh. Decrease to increase the quality of the VQ.
        h = .02  # point in the mesh [x_min, x_max]x[y_min, y_max].

        # Plot the decision boundary. For that, we will assign a color to each
        x_min, x_max = reduced_data[:, 0].min() - 1, reduced_data[:, 0].max() + 1
        y_min, y_max = reduced_data[:, 1].min() - 1, reduced_data[:, 1].max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

        # Obtain labels for each point in mesh. Use last trained model.
        Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])

        # Put the result into a color plot
        Z = Z.reshape(xx.shape)
        plt.figure(1)
        plt.clf()
        plt.imshow(Z, interpolation='nearest',
                   extent=(xx.min(), xx.max(), yy.min(), yy.max()),
                   cmap=plt.cm.Paired,
                   aspect='auto', origin='lower')

        plt.plot(reduced_data[:, 0], reduced_data[:, 1], 'k.', markersize=2)
        # Plot the centroids as a white X
        centroids = kmeans.cluster_centers_
        plt.scatter(centroids[:, 0], centroids[:, 1],
                    marker='x', s=169, linewidths=3,
                    color='w', zorder=10)
        plt.title('K-means clustering on the digits dataset (PCA-reduced data)\n'
                  'Centroids are marked with white cross')
        plt.xlim(x_min, x_max)
        plt.ylim(y_min, y_max)
        plt.xticks(())
        plt.yticks(())
        plt.show()
        print(y)
        print(kmeans.labels_)
        print(centroids)
        print('-'*80)
        reduced_data = PCA(n_components=100).fit_transform(data)
        kmeans = KMeans(init='k-means++', n_clusters=n_digits, n_init=10)
        kmeans.fit(reduced_data)
        print(y)
        print(kmeans.labels_)
        centroids = kmeans.cluster_centers_
        print(centroids)

