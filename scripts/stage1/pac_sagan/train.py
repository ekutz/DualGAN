import torch
import torch.nn as nn
from stage1.pac_sagan.model import Generator, Discriminator, DiscriminatorUnet
import torch.optim as optim
import torchvision.utils as vutils
import matplotlib.pyplot as plt
import socket
import numpy as np
from typing import Union, Optional, Sequence, Tuple, Text, BinaryIO
import math
from PIL import Image
import os
from torch.autograd import Variable
from stage1.data_utils.image_pool import ImagePool
import stage1.metric as metric
from stage1.metric import make_dataset

DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro-2".lower()) else False
MODEL_PATH = '../Models/s1/'


def tile(a, dim, n_tile):
    init_dim = a.size(dim)
    repeat_idx = [1] * a.dim()
    repeat_idx[dim] = n_tile
    a = a.repeat(*(repeat_idx))
    order_index = torch.LongTensor(np.concatenate([init_dim * np.arange(n_tile) + i for i in range(init_dim)]))
    return torch.index_select(a, dim, order_index)


def weights_init(m):
    classname = m.__class__.__name__
    if isinstance(m, nn.Conv2d):
        if hasattr(m, 'weight'):
            nn.init.normal_(m.weight.data, 0.0, 0.02)
    if isinstance(m, nn.BatchNorm2d):
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)


def normalize(tensor, range: Optional[Tuple[int, int]] = None, scale_each: bool = False):
    tensor = tensor.clone()  # avoid modifying tensor in-place
    if range is not None:
        assert isinstance(range, tuple), \
            "range has to be a tuple (min, max) if specified. min and max are numbers"

    def norm_ip(img, min, max):
        img.clamp_(min=min, max=max)
        img.add_(-min).div_(max - min + 1e-5)

    def norm_range(t, range):
        if range is not None:
            norm_ip(t, range[0], range[1])
        else:
            norm_ip(t, float(t.min()), float(t.max()))

    if scale_each is True:
        for t in tensor:  # loop over mini-batch dimension
            norm_range(t, range)
    else:
        norm_range(tensor, range)
    return tensor


def save_image(image, path, ext):
    im = Image.fromarray(image)
    im = im.convert("L")
    im.save(path + '.' + ext, ext.upper())


def interpolate_points(p1, p2, n_steps=10):
    # interpolate ratios between the points
    ratios = np.linspace(0, 1, num=n_steps)
    # linear interpolate vectors
    vectors = list()
    for ratio in ratios:
        v = (1.0 - ratio) * p1 + ratio * p2
        vectors.append(v)
    return np.asarray(vectors)


class SaganInference:
    def __init__(self, args):
        self.args = args

        self.device = torch.device("cuda:0" if (torch.cuda.is_available() and self.args.ngpu > 0) else "cpu")
        # init generator
        self.netG = Generator(image_size=self.args.image_size, nz=self.args.nz, ngf=self.args.ngf, nc=self.args.nc).to(
            self.device)
        if self.args.tsne_plot:
            self.netD = Discriminator(image_size=self.args.image_size, ndf=self.args.ndf, nc=self.args.nc,
                                      pac_fac=self.args.pac_factor, adding_noise=self.args.add_noise,
                                      tsne_plot=self.args.tsne_plot).to(self.device)

        if self.device.type == 'cpu':

            checkpoint = torch.load(self.args.model_path, map_location=lambda storage, location: storage)
        else:
            checkpoint = torch.load(self.args.model_path)

        # load generator or discriminator
        if self.args.tsne_plot:
            self.netD.load_state_dict(checkpoint['netD_state_dict'])
        else:
            self.netG.load_state_dict(checkpoint['netG_state_dict'])

    def discriminator_raw_output(self, image_batch):
        real_images = image_batch.to(self.device)
        real_images_stacked = self.get_stacked_images_test(real_images)
        x = self.netD(real_images_stacked)
        return x

    def inference(self):
        total_runs = int(math.ceil(self.args.gen_amount / self.args.batch_size))
        n = 0
        if self.args.inf_out:
            os.makedirs(os.path.join(self.args.inf_out), exist_ok=True)
        else:
            os.makedirs('fake', exist_ok=True)
        for i in range(total_runs):
            noise = torch.randn(self.args.batch_size, self.args.nz, 1, 1, device=self.device)

            fake, _, _ = self.netG(noise)

            fake = normalize(fake.detach().cpu()).numpy()
            for f in fake:
                if n >= self.args.gen_amount:
                    break
                image = f[0] * 255
                # if DEBUG:
                #     plt.imshow(image)
                #     plt.show()
                fname = "fake" + str(n)
                if self.args.inf_out is not None:
                    save_image(image, os.path.join(self.args.inf_out, fname), ext=self.args.extension)
                else:
                    save_image(image, os.path.join('fake', fname), ext=self.args.extension)
                n += 1

    def interpolate_latent_space(self):
        # create two points
        os.makedirs(self.args.inf_out, exist_ok=True)
        for i in range(self.args.gen_amount):
            n_steps = 8
            noise = torch.randn(2, self.args.nz, 1, 1)

            #  interpolate between the two points
            interpolated = interpolate_points(noise[0, :, 0, 0].numpy(), noise[1, :, 0, 0].numpy(), n_steps=n_steps)
            interpolated = torch.from_numpy(interpolated[:, :, np.newaxis, np.newaxis]).to(self.device)
            # predict batch of interpolated points
            fake, _, _ = self.netG(interpolated)
            fake = fake.detach().cpu()
            # plot images
            img = vutils.make_grid(fake, padding=2, normalize=True, nrow=n_steps)

            # if DEBUG:
            plt.figure(figsize=(2 * 10, 3))
            plt.axis("off")
            fake_images = np.transpose(img, (1, 2, 0))
            plt.imshow(fake_images)
            plt.tight_layout()
            plt.savefig(os.path.join(self.args.inf_out, 'interpolated_' + str(i) + '.png'), transparent=True)
            plt.close()
            # plt.show()
            # else:
            #     pass

    def get_stacked_images_test(self, data):
        # initially skipping unfull batches
        if len(data.shape) < 4:
            stacked_data = tile(data.unsqueeze(0).unsqueeze(0), 1, self.args.pac_factor)
        else:
            # stacked_data = torch.zeros((data.size(0),
            #                             data.size(1) * self.args.pac_factor,
            #                             data.size(2),
            #                             data.size(3)),
            #                            device=self.device)

            stacked_data = tile(data, 1, self.args.pac_factor).to(self.device)
        # for i in range(stacked_data.shape[0]):
        #     ix_temp = ix[n:n + self.args.pac_factor]
        #     stacked_data[i] = data[ix_temp].permute(1, 0, 2, 3)
        #     n += self.args.pac_factor
        return stacked_data


class SaganTraining:
    def __init__(self, args, dataloader, writer=None):
        print(
            "batch_size: " + str(args.batch_size) + " lr: " + str(args.lr) + " ngf: " + str(args.ngf) + " ndf: " + str(
                args.ndf) + " pac_factor: " + str(args.pac_factor))
        self.args = args
        self.dataloader = dataloader
        self.device = torch.device("cuda:0" if (torch.cuda.is_available() and self.args.ngpu > 0) else "cpu")

        # Create the generator
        self.netG = Generator(image_size=self.args.image_size, nz=self.args.nz, ngf=self.args.ngf, nc=self.args.nc).to(
            self.device)

        # Handle multi-gpu if desired
        if (self.device.type == 'cuda') and (self.args.ngpu > 1):
            self.netG = nn.DataParallel(self.netG, list(range(self.args.ngpu)))

        # Apply the weights_init function to randomly initialize all weights
        #  to mean=0, stdev=0.2.
        self.netG.apply(weights_init)

        # Print the model
        if self.args.mode == 'train':
            print(self.netG)

        # Create the Discriminator
        if self.args.dis_net == 'Conv':

            self.netD = Discriminator(image_size=self.args.image_size, ndf=self.args.ndf, nc=self.args.nc,
                                      pac_fac=self.args.pac_factor, adding_noise=self.args.add_noise).to(
                self.device)
        elif self.args.dis_net == 'Unet':
            self.netD = DiscriminatorUnet(image_size=self.args.image_size, ndf=self.args.ndf, nc=self.args.nc,
                                          pac_fac=self.args.pac_factor).to(
                self.device)  # Handle multi-gpu if desired
        if (self.device.type == 'cuda') and (self.args.ngpu > 1):
            self.netD = nn.DataParallel(self.netD, list(range(self.args.ngpu)))

        # Apply the weights_init function to randomly initialize all weights
        #  to mean=0, stdev=0.2.
        self.netD.apply(weights_init)

        # Print the model
        if self.args.mode == 'train':
            print(self.netD)
        # %%

        # Initialize BCELoss function
        # self.criterion = nn.BCELoss()
        self.c_loss = torch.nn.CrossEntropyLoss()
        self.BCE_stable = torch.nn.BCEWithLogitsLoss()

        # Create batch of latent vectors that we will use to visualize
        #  the progression of the generator
        self.fixed_noise = torch.randn(64, self.args.nz, 1, 1, device=self.device)
        self.fixed_noise_fid = torch.randn(100, self.args.nz, 1, 1, device=self.device)
        # Setup Adam optimizers for both G and D
        self.optimizerD = optim.Adam(self.netD.parameters(), lr=self.args.lr, betas=(self.args.beta1, 0.999))
        self.optimizerG = optim.Adam(self.netG.parameters(), lr=self.args.lr, betas=(self.args.beta1, 0.999))

        # Logging
        self.writer = writer
        # [emd-mmd-knn(knn,real,fake,precision,recall)]*4 - IS - mode_score - FID
        if self.args.use_metric:
            self.score_tr = np.zeros((args.epochs, 4 * 7 + 3))

            # compute initial score
            s = metric.compute_score_raw(args.dataset, args.image_size, args.dataroot, args.sampleSize, 16,
                                         args.outf + '/real/',
                                         args.outf + '/fake/',
                                         self.netG, args.nz, conv_model='inception_v3', workers=int(args.workers),
                                         sagan=True)
            self.score_tr[0] = s
            np.save('%s/score_tr.npy' % (self.args.outf), self.score_tr)
        self.lambda_gp = 10

        self.fake_pool = ImagePool(self.args.pool_size)

    def get_stacked_images(self, data):
        stacked_data = None
        # initially skipping unfull batches
        ix = np.random.choice(data.size(0), data.size(0), False)
        stacked_data = torch.zeros((int(data.size(0) / self.args.pac_factor),
                                    data.size(1) * self.args.pac_factor,
                                    data.size(2),
                                    data.size(3)),
                                   device=self.device)
        n = 0
        for i in range(stacked_data.shape[0]):
            ix_temp = ix[n:n + self.args.pac_factor]
            stacked_data[i] = data[ix_temp].permute(1, 0, 2, 3)
            n += self.args.pac_factor
        return stacked_data

    def train(self):
        # Establish convention for real and fake labels during training
        real_label = 1
        fake_label = 0
        # Training Loop

        # Lists to keep track of progress
        G_losses = []
        D_losses = []
        iters = 0
        factor = 0.5
        if self.args.mode == 'train':
            print("Starting Training Loop...")

        for epoch in range(self.args.epochs):
            # For each batch in the dataloader
            for i, data in enumerate(self.dataloader, 0):
                self.netD.train()
                self.netG.train()

                real_images = data[0]

                # Compute loss with real images
                # dr1, dr2, df1, df2, gf1, gf2 are attention scores
                real_images = real_images.to(self.device)
                real_images_stacked = self.get_stacked_images(real_images)
                # if self.args.dis_net == 'Conv':
                d_out_real, dr1, dr2 = self.netD(real_images_stacked)
                if self.args.adv_loss == 'wgan-gp':
                    d_loss_real = - torch.mean(d_out_real)
                elif self.args.adv_loss == 'hinge':
                    d_loss_real = torch.nn.ReLU()(1.0 - d_out_real).mean()

                # apply Gumbel Softmax
                z = torch.randn(real_images.size(0), self.args.nz, device=self.device)
                fake_images, gf1, gf2 = self.netG(z)
                # if self.args.pac_factor>1:
                if self.args.use_pool:
                    fake_images, gf1, gf2 = self.fake_pool.query(fake_images, gf1, gf2)
                fake_images_stacked = self.get_stacked_images(fake_images)
                # if self.args.dis_net == 'Conv':
                d_out_fake, df1, df2 = self.netD(fake_images_stacked)
                if self.args.adv_loss == 'wgan-gp':
                    d_loss_fake = d_out_fake.mean()
                elif self.args.adv_loss == 'hinge':
                    d_loss_fake = torch.nn.ReLU()(1.0 + d_out_fake).mean()
                # elif self.args.dis_net == 'Unet':
                #     d_out_fake, df1, df2, d_out_fake_dec = self.netD(fake_images_stacked)
                #     d_out_fake_dec = d_out_fake_dec.squeeze()
                #     if self.args.adv_loss == 'wgan-gp':
                #         d_loss_fake = d_out_fake.mean()
                #         d_loss_fake_dec = d_out_fake_dec.view(d_out_fake_dec.shape[0], -1).mean(dim=1).mean()*factor
                #     elif self.args.adv_loss == 'hinge':
                #         d_loss_fake = torch.nn.ReLU()(1.0 + d_out_fake).mean()

                # Backward + Optimize
                # if self.args.dis_net == 'Conv':
                d_loss = d_loss_real + d_loss_fake
                # elif self.args.dis_net == 'Unet':
                #     d_loss = d_loss_real + d_loss_fake
                #     d_loss_dec = d_loss_real_dec + d_loss_fake_dec
                #     if DEBUG:
                #         print(d_loss,d_loss_dec)
                #     d_loss += d_loss_dec
                self.netG.zero_grad()
                self.netD.zero_grad()
                d_loss.backward()
                self.optimizerD.step()

                if self.args.adv_loss == 'wgan-gp':
                    # Compute gradient penalty
                    alpha = torch.rand(real_images_stacked.size(0), 1, 1, 1).to(self.device).expand_as(
                        real_images_stacked)
                    interpolated = Variable(alpha * real_images_stacked.data + (1 - alpha) * fake_images_stacked.data,
                                            requires_grad=True)
                    # if self.args.pac_factor > 1:
                    #    interpolated = self.get_stacked_images(interpolated)
                    if self.args.dis_net == 'Conv':
                        out, _, _ = self.netD(interpolated)
                    elif self.args.dis_net == 'Unet':
                        out, _, _, _ = self.netD(interpolated)

                    grad = torch.autograd.grad(outputs=out,
                                               inputs=interpolated,
                                               grad_outputs=torch.ones(out.size()).to(self.device),
                                               retain_graph=True,
                                               create_graph=True,
                                               only_inputs=True)[0]

                    grad = grad.view(grad.size(0), -1)
                    grad_l2norm = torch.sqrt(torch.sum(grad ** 2, dim=1))
                    d_loss_gp = torch.mean((grad_l2norm - 1) ** 2)

                    # Backward + Optimize
                    d_loss = self.lambda_gp * d_loss_gp

                    self.netG.zero_grad()
                    self.netD.zero_grad()
                    d_loss.backward()
                    self.optimizerD.step()

                # ================== Train G and gumbel ================== #
                # Create random noise
                z = torch.randn(real_images.size(0), self.args.nz).to(self.device)
                fake_images, _, _ = self.netG(z)
                # Compute loss with fake images
                # if self.args.pac_factor>1:
                fake_images_stacked = self.get_stacked_images(fake_images)
                if self.args.dis_net == 'Conv':
                    g_out_fake, _, _ = self.netD(fake_images_stacked)
                    g_loss_fake = - g_out_fake.mean()
                elif self.args.dis_net == 'Unet':
                    g_out_fake, _, _, g_out_fake_dec = self.netD(fake_images_stacked)  # batch x n
                    g_out_fake_dec = g_out_fake_dec.squeeze()
                    g_loss_fake = - g_out_fake.mean()
                    g_loss_fake -= g_out_fake_dec.view(g_out_fake_dec.shape[0], -1).mean(dim=1).mean() * factor

                self.netG.zero_grad()
                self.netD.zero_grad()
                g_loss_fake.backward()
                self.optimizerG.step()
                if self.args.mode == 'train':
                    # Output training stats
                    # if i % 50 == 0:
                    #     print('[%d/%d][%d/%d]\tLoss_D: %.4f\tLoss_G: %.4f\tD(x): %.4f\tD(G(z)): %.4f / %.4f'
                    #           % (epoch, self.args.epochs, i, len(self.dataloader),
                    #              errD.item(), errG.item(), D_x, D_G_z1, D_G_z2))

                    # Save Losses for plotting later
                    # G_losses.append(errG.item())
                    # D_losses.append(errD.item())
                    if self.writer:
                        self.writer.add_scalar('Loss/errG', g_loss_fake.item(), iters)
                        self.writer.add_scalar('Loss/errD', d_loss.item(), iters)

                # Check how the generator is doing by saving G's output on fixed_noise
                if self.writer:
                    if (iters % 200 == 0) or ((epoch == self.args.epochs - 1) and (i == len(self.dataloader) - 1)):
                        with torch.no_grad():
                            # self.netG.eval()
                            fake, _, _ = self.netG(self.fixed_noise)
                            fake = fake.detach().cpu()
                            img = vutils.make_grid(fake, padding=2, normalize=True)
                            # Plot the fake images from the last epoch
                            self.writer.add_images('fake_images', img.numpy()[np.newaxis], iters)
                            if self.args.dataset == 'gland':
                                self.writer.add_images('fake_images_bin', img.numpy()[np.newaxis] > 0, iters)

                        if DEBUG:
                            plt.figure(figsize=(15, 15))
                            plt.axis("off")
                            plt.title("Fake Images")
                            fake_images = np.transpose(img, (1, 2, 0))
                            plt.imshow(fake_images)
                            plt.show()
                        if self.args.use_metric:
                            ################################################
                            #### metric scores computing (key function) ####
                            ################################################
                            s = metric.compute_score_raw(self.args.dataset, self.args.image_size, self.args.dataroot,
                                                         self.args.sampleSize, self.args.batch_size,
                                                         self.args.outf + '/real/', self.args.outf + '/fake/',
                                                         self.netG, self.args.nz, conv_model='inception_v3',
                                                         workers=int(self.args.workers), epoch=epoch,
                                                         sagan=True)
                            self.score_tr[epoch] = s
                            print(s)
                            self.writer.add_scalar('Metric/inception_score', s[28], iters)
                            self.writer.add_scalar('Metric/mode_score', s[29], iters)
                            self.writer.add_scalar('Metric/fid', s[30], iters)
                iters += 1
                if DEBUG:
                    print(iters)
            if self.args.save_model_step > 0:
                if epoch % self.args.save_model_step == 0 and epoch > 0:
                    self.save_models(epoch=epoch)
        if self.args.use_metric:
            np.save('%s/score_tr_ep.npy' % self.args.outf, self.score_tr)
        # if self.args.mode == 'optimize':
        #     fake = self.netG(self.fixed_noise_fid).detach().cpu()
        #     return normalize(fake).numpy()

    def save_models(self, epoch):
        # save_path = '{}/{}/epoch_{}.pt'.format(MODEL_PATH, self.args.now, epoch)
        save_path = os.path.join(MODEL_PATH, self.args.now.strftime("%b%d_%H-%M-%S"))
        # if not os.path.exists(save_path):
        os.makedirs(save_path, exist_ok=True)
        save_path = os.path.join(save_path, str(epoch))
        torch.save({
            'epoch': epoch,
            'netG_state_dict': self.netG.state_dict(),
            'netD_state_dict': self.netD.state_dict(),
            # 'optimizerD_state_dict': self.optimizerD.state_dict(),
            # 'optimizerG_state_dict': self.optimizerG.state_dict(),
        }, save_path)
