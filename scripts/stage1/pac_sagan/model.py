import torch.nn as nn
from stage1.data_utils.SpectralNorm import SpectralNorm
from stage1.data_utils.SelfAttn import SelfAttn
from stage1.data_utils.GaussianLayer import GaussianNoise
from stage1.data_utils.utils import *
import matplotlib.pyplot as plt


class Generator(nn.Module):
    """
    Generator model
    The model is created to be used for different images sizes.
    Image sizes that work are [64,128,256,512] default: 64
    """

    def __init__(self, image_size=64, nz=100, ngf=64, nc=3):
        super(Generator, self).__init__()
        self.image_size = image_size
        # used dimensions of the different layers - used in attn layer
        dim = {}
        first = []
        layer_mid = {}
        last = []

        def block(input_channel, output_channel, kernel_size=4, stride=1, padding=0):
            layer = [SpectralNorm(nn.ConvTranspose2d(input_channel, output_channel, kernel_size, stride, padding)),
                     nn.BatchNorm2d(output_channel), nn.ReLU()]
            return layer

        repeat_num = int(np.log2(self.image_size)) - 3  # amount of blocks depends on the image size

        mult = 2 ** repeat_num  # 8
        curr_dim = ngf * mult
        dim[0] = curr_dim
        first = block(nz, curr_dim, 4)

        for rep in range(repeat_num):
            layer_mid[rep] = block(curr_dim, int(curr_dim / 2), 4, 2, 1)
            curr_dim = int(curr_dim / 2)
            dim[rep + 1] = curr_dim

        if self.image_size >= 64:
            self.l1 = nn.Sequential(*first)
            self.l2 = nn.Sequential(*layer_mid[0])
            self.l3 = nn.Sequential(*layer_mid[1])
            self.l4 = nn.Sequential(*layer_mid[2])
        if self.image_size >= 128:
            self.l5 = nn.Sequential(*layer_mid[3])
        if self.image_size >= 256:
            self.l6 = nn.Sequential(*layer_mid[4])
        if self.image_size >= 512:
            self.l7 = nn.Sequential(*layer_mid[5])
        last.append(nn.ConvTranspose2d(curr_dim, nc, 4, 2, 1))
        last.append(nn.Tanh())
        self.last = nn.Sequential(*last)

        self.attn1 = SelfAttn(dim[2], 'relu')
        self.attn2 = SelfAttn(dim[3], 'relu')

    def forward(self, z):
        z = z.view(z.size(0), z.size(1), 1, 1)
        if self.image_size >= 64:
            out = self.l1(z)
            out = self.l2(out)
            out = self.l3(out)
            out, p1 = self.attn1(out)
            # plt.imshow(p1[0].clone().detach())
            # plt.show()
            out = self.l4(out)
            out, p2 = self.attn2(out)
            # plt.imshow(p2[0].clone().detach())
            plt.show()
        if self.image_size >= 128:
            out = self.l5(out)
        if self.image_size >= 256:
            out = self.l6(out)
        if self.image_size >= 512:
            out = self.l7(out)
        out = self.last(out)

        return out, p1, p2


class Discriminator(nn.Module):
    """Discriminator, Auxiliary Classifier."""

    def __init__(self, image_size=64, ndf=64, nc=3, pac_fac=1, adding_noise=True, tsne_plot=False):
        super(Discriminator, self).__init__()
        self.tsne_plot = tsne_plot
        self.image_size = image_size
        self.adding_noise = adding_noise
        repeat_num = int(np.log2(self.image_size)) - 3

        dim = {}
        layer_mid = {}
        last = []

        def block(input_channel, output_channel, kernel_size=4, stride=2, padding=1):
            layer = [SpectralNorm(nn.Conv2d(input_channel, output_channel, kernel_size, stride, padding)),
                     nn.LeakyReLU(0.1)]
            return layer

        curr_dim = ndf
        dim[0] = curr_dim
        # create layer in sperated list in dict
        first = block(nc * pac_fac, curr_dim, 4, 2, 1)
        for rep in range(repeat_num):
            layer_mid[rep] = block(curr_dim, curr_dim * 2, 4, 2, 1)
            curr_dim = curr_dim * 2
            dim[rep + 1] = curr_dim
        last.append(nn.Conv2d(curr_dim, 1, 4))

        # transfer layer from list to class variables
        if self.image_size >= 64:
            self.l1 = nn.Sequential(*first)
            self.l2 = nn.Sequential(*layer_mid[0])
            self.l3 = nn.Sequential(*layer_mid[1])
            self.attn1 = SelfAttn(dim[2], 'relu')
            self.l4 = nn.Sequential(*layer_mid[2])
            self.attn2 = SelfAttn(dim[3], 'relu')
        if self.image_size >= 128:
            self.l5 = nn.Sequential(*layer_mid[3])
        if self.image_size >= 256:
            self.l6 = nn.Sequential(*layer_mid[4])
        if self.image_size >= 512:
            self.l7 = nn.Sequential(*layer_mid[5])
        self.last = nn.Sequential(*last)
        self.gaussian_noise = GaussianNoise()

    def forward(self, x):
        if self.image_size >= 64:
            x = self.gaussian_noise(x) if self.adding_noise else x
            x = self.l1(x)
            x = self.gaussian_noise(x) if self.adding_noise else x
            x = self.l2(x)
            x = self.gaussian_noise(x) if self.adding_noise else x
            x = self.l3(x)
            x, p1 = self.attn1(x)
            # plt.imshow(p1[0].clone().detach())
            # plt.show()
            x = self.gaussian_noise(x) if self.adding_noise else x
            x = self.l4(x)
            x, p2 = self.attn2(x)
            # plt.imshow(p2[0].clone().detach())
            # plt.show()
            x = self.gaussian_noise(x) if self.adding_noise else x
        if self.image_size >= 128:
            x = self.l5(x)
            x = self.gaussian_noise(x) if self.adding_noise else x
        if self.image_size >= 256:
            x = self.l6(x)
            x = self.gaussian_noise(x) if self.adding_noise else x
        if self.image_size >= 512:
            x = self.l7(x)
            x = self.gaussian_noise(x) if self.adding_noise else x

        out = self.last(x)
        if self.tsne_plot:
            return x
        return out.squeeze(), p1, p2


class UpBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(UpBlock, self).__init__()
        self.up_sample = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
        self.double_conv = nn.Sequential(*[
            nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1),
            nn.LeakyReLU(0.1)
        ])

    def forward(self, down_input, skip_input):
        x = self.up_sample(down_input)
        x = torch.cat([x, skip_input], dim=1)
        return self.double_conv(x)


class DownBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(DownBlock, self).__init__()
        self.double_conv = SpectralNorm(nn.Conv2d(in_channels, out_channels, kernel_size=3, stride=1, padding=1))
        self.lr = nn.LeakyReLU(0.1)
        self.down_sample = nn.MaxPool2d(2)

    def forward(self, x):
        skip_out = self.lr(self.double_conv(x))
        down_out = self.down_sample(skip_out)
        return down_out, skip_out


class DiscriminatorUnet(nn.Module):
    """Discriminator, Auxiliary Classifier."""

    def __init__(self, image_size=64, ndf=64, nc=3, pac_fac=1):
        super(DiscriminatorUnet, self).__init__()
        self.image_size = image_size
        self.first = nn.Conv2d(nc * pac_fac, ndf, kernel_size=3, padding=1)
        self.en1 = DownBlock(ndf, ndf)
        self.en2 = DownBlock(ndf, ndf * 2)
        self.en3 = DownBlock(ndf * 2, ndf * 4)
        self.en4 = DownBlock(ndf * 4, ndf * 8)
        self.en5 = DownBlock(ndf * 8, ndf * 8)
        self.en6 = DownBlock(ndf * 8, ndf * 8)
        # self.en7 = self.downblock(ndf * 8, ndf * 8)

        self.attnEn1 = SelfAttn(ndf * 4, 'relu')
        self.attnEn2 = SelfAttn(ndf * 8, 'relu')
        self.attnDe2 = SelfAttn(ndf * 8, 'relu')
        self.attnDe1 = SelfAttn(ndf * 4, 'relu')
        self.bottleneck = nn.Conv2d(ndf * 8, ndf * 8, kernel_size=3, padding=1)

        self.bottleneck_out = nn.Sequential(*[
            nn.Conv2d(ndf * 8, 1, 1),
            nn.AvgPool2d(4)
        ])

        # self.de7 =self.upblock(ndf*8+ndf*8, ndf*8)
        self.de6 = UpBlock(ndf * 8 + ndf * 8, ndf * 8)
        self.de5 = UpBlock(ndf * 8 + ndf * 8, ndf * 8)
        self.de4 = UpBlock(ndf * 8 + ndf * 8, ndf * 8)
        self.de3 = UpBlock(ndf * 4 + ndf * 8, ndf * 4)
        self.de2 = UpBlock(ndf * 2 + ndf * 4, ndf * 2)
        self.de1 = UpBlock(ndf + ndf * 2, ndf)
        self.decoder_out = nn.Conv2d(ndf, 1, kernel_size=1)

    def forward(self, x):
        x = self.first(x)
        x, skip1_out = self.en1(x)
        x, skip2_out = self.en2(x)
        x, skip3_out = self.en3(x)
        x, p1 = self.attnEn1(x)
        x, skip4_out = self.en4(x)
        x, p2 = self.attnEn2(x)
        x, skip5_out = self.en5(x)
        x, skip6_out = self.en6(x)
        encoder_out = self.bottleneck_out(x)
        x = self.bottleneck(x)
        x = self.de6(x, skip6_out)
        x = self.de5(x, skip5_out)
        x = self.de4(x, skip4_out)
        x, _ = self.attnDe2(x)
        x = self.de3(x, skip3_out)
        x, _ = self.attnDe1(x)
        x = self.de2(x, skip2_out)
        x = self.de1(x, skip1_out)
        decoder_out = self.decoder_out(x)

        return encoder_out.squeeze(), p1, p2, decoder_out
