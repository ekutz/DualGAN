from __future__ import print_function
# %matplotlib inline
import random
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
import numpy as np
import matplotlib.pyplot as plt
import socket
from stage1.config import *
from datetime import datetime
from torch.utils.tensorboard import SummaryWriter
from stage1.dcgan.train import DcganTraining, DcganInference
from fid.fid import FID, calculate_frechet_distance
from collections import OrderedDict
from hyperopt import fmin, atpe, Trials, hp
from stage1.data_utils.utils import ObjectView

DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro".lower()) else False

if DEBUG:
    DATAROOT = "/Users/eike/Datasets/img_align_celebb"
    DATAROOT_MESSIDOR = "/Users/eike/Datasets/Messidor/Base11_anno/"
else:
    DATAROOT = "~/Datasets/celeba"
    DATAROOT_MESSIDOR = "~/Datasets/Messidor/train/"

DATAROOT_MNIST = "data/mnist"


def objective(args):
    """Objective function for Bayesian optimization
    """
    global dataloader
    args_temp = ObjectView(args)
    dataloader = args_temp.dataloader
    fid = args_temp.fid

    del args_temp.dataloader
    del args_temp.fid
    # create model based on given values
    trainer = DcganTraining(args_temp, dataloader)

    # trainer = TrainerOne(dataloader, args_temp.config, args_temp, args_temp.mean, args_temp.std, masks_np=masks_data,
    #                     fid=fid)
    fake_batch = trainer.train()
    if not DEBUG:
        m2, s2 = fid.calculate_activation_statistics_numpy(fake_batch)
        return calculate_frechet_distance(fid.m1, fid.s1, m2, s2)
    else:
        return random.random(0., 10.)


if __name__ == '__main__':
    now = datetime.now()
    writer = SummaryWriter(flush_secs=1, log_dir='runs_stage1/' + now.strftime("%b%d_%H-%M-%S") + '/')
    args = get_main_values()
    args.now = now

    # Set random seed for reproducibility
    if args.mode == 'train' or args.mode == 'optimize':
        manualSeed = 999
    else:
        manualSeed = random.randint(1, 10000)  # use if you want new results
    print("Random Seed: ", manualSeed)
    random.seed(manualSeed)
    torch.manual_seed(manualSeed)

    # We can use an image folder dataset the way we have it setup.
    # Create the dataset
    if args.mode == 'train' or args.mode == 'optimize':
        dataset = None
        if args.dataset == 'celeba':
            dataset = dset.ImageFolder(root=DATAROOT,
                                       transform=transforms.Compose([
                                           transforms.Resize(args.image_size),
                                           transforms.CenterCrop(args.image_size),
                                           transforms.ToTensor(),
                                           transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                                       ]))
            if DEBUG:
                fid = None
            else:
                fid = FID(path=DATAROOT)
        elif args.dataset == 'messidor':
            dataset = dset.ImageFolder(root=DATAROOT_MESSIDOR,

                                       transform=transforms.Compose([
                                           transforms.Grayscale(),
                                           transforms.Resize(args.image_size),
                                           transforms.CenterCrop(args.image_size),
                                           transforms.ToTensor(),
                                           transforms.Normalize([0.5], [0.5]),
                                       ]))
            args.nc = 1
            if DEBUG:
                fid = None
            else:
                fid = FID(path='/zhome/95/c/135723/Datasets/Messidor/train/train_512')
        elif args.dataset == 'mnist':
            dataset = dset.MNIST(root=DATAROOT_MNIST, train=True, download=True,
                                 transform=transforms.Compose([
                                     transforms.Resize(args.image_size),
                                     transforms.CenterCrop(args.image_size),
                                     transforms.ToTensor(),
                                     transforms.Normalize([0.5], [0.5]),
                                 ]))
            args.nc = 1
            if DEBUG:
                fid = None
            else:
                fid = FID(path=DATAROOT_MNIST)
        # Create the dataloader

        dataloader = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=True,
                                                 num_workers=args.workers)
    # Decide which device we want to run on
    device = torch.device("cuda:0" if (torch.cuda.is_available() and args.ngpu > 0) else "cpu")
    if args.mode == 'train' or args.mode == 'optimize':

        # Plot some training images
        real_batch = next(iter(dataloader))
        plt.figure(figsize=(8, 8))
        plt.axis("off")
        plt.title("Training Images")
        mask = vutils.make_grid(real_batch[0].to(device)[:64], padding=2, normalize=True).cpu()

        masks = np.transpose(mask, (1, 2, 0))
        plt.imshow(masks)
        if DEBUG:
            plt.show()
    if args.mode == 'train':

        writer = SummaryWriter(flush_secs=1, log_dir='runs_stage1/' + now.strftime("%b%d_%H-%M-%S") + '/')
        writer.add_images('training_images', mask.numpy()[np.newaxis], 0)
        trainer = DcganTraining(args, dataloader, writer)
        trainer.train()
    elif args.mode == 'optimize':
        # writer = SummaryWriter(flush_secs=1, log_dir='runs_stage1_bayopt/' + now.strftime("%b%d_%H-%M-%S") + '/')
        SPACE = OrderedDict([
            ('mode', args.mode),
            ('epochs', args.epochs),
            ('image_size', args.image_size),
            ('batch_size', hp.randint('batch_size', 35) + 5),
            ('ndf', hp.choice('ndf', [64, 32, 16])),
            ('ngf', hp.choice('ngf', [128, 64, 32])),
            ('lr', hp.loguniform('lr', np.log(0.00001), np.log(0.0002))),
            ('nz', args.nz),
            ('dataloader', dataloader),
            ('nc', args.nc),
            ('beta1', args.beta1),
            ('ngpu', args.ngpu),
            ('fid', fid),
            ('save_model_step', args.save_model_step)
        ])
        trials = Trials()
        best = fmin(objective, SPACE, algo=atpe.suggest, max_evals=args.max_evals, trials=trials)
        with SummaryWriter(log_dir='runs_stage1_bayopt/' + now.strftime("%b%d_%H-%M-%S") + '/') as w:
            for i in range(len(trials.results)):
                hyperparams = {}
                for key in trials.vals.keys():
                    if isinstance(trials.vals[key][i], np.int64):
                        hyperparams[key] = int(trials.vals[key][i])
                    elif isinstance(trials.vals[key][i], np.float64):
                        hyperparams[key] = float(trials.vals[key][i])
                    else:
                        hyperparams[key] = trials.vals[key][i]
                w.add_hparams(hyperparams,
                              {'hparam/FID': trials.results[i]['loss']})
        print(best)

    elif args.mode == 'inference':
        inference = DcganInference(args)
        inference.inference()
