import os
import torch
from torch.autograd import Variable
import numpy as np


def tensor2var(x, grad=False):
    if torch.cuda.is_available():
        x = x.cuda()
    return Variable(x, requires_grad=grad)


def var2tensor(x):
    return x.data.cpu()


def var2numpy(x):
    return x.data.cpu().numpy()


def denorm(x):
    out = (x + 1) / 2
    return out.clamp_(0, 1)


def masks_to_numpy(masks):
    X = np.array(masks, np.float32)
    return X


def str2bool(v):
    return v.lower() in ('true')


class ObjectView(object):
    def __init__(self, d):
        self.nz = None
        self.dataroot = None
        self.image_size = None
        self.outf = None
        self.sampleSize = None
        self.workers = None
        self.batch_size = None
        self.dataset = None
        self.dataloader = None
        self.mask_data = None
        self.fid = None
        self.data_loader_val = None
        self.data_loader_train = None
        self.std = None
        self.mean = None
        self.config = None
        self.adv_loss = None
        self.dis_net = None
        self.add_noise = None
        self.use_pool = None
        self.pool_size = None
        self.__dict__ = d
