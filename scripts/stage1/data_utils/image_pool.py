import random
import torch
from torch.autograd import Variable


class ImagePool:
    def __init__(self, pool_size):
        self.pool_size = pool_size
        if self.pool_size > 0:
            self.num_imgs = 0
            self.images = []
            self.gf1 = []
            self.gf2 = []

    def query(self, images, gf1, gf2):
        if self.pool_size == 0:
            return images
        return_images = []
        return_gf1 = []
        return_gf2 = []
        for idx, image in enumerate(images.data):
            image = torch.unsqueeze(image, 0)
            if self.num_imgs < self.pool_size:
                self.num_imgs = self.num_imgs + 1
                self.images.append(image)
                self.gf1.append(gf1[idx])
                self.gf2.append(gf2[idx])
                return_images.append(image)
                return_gf1.append(gf1[idx])
                return_gf2.append(gf2[idx])
            else:
                p = random.uniform(0, 1)
                if p > 0.5:
                    random_id = random.randint(0, self.pool_size - 1)
                    tmp = self.images[random_id].clone()
                    self.images[random_id] = image
                    tmp_gf1 = self.gf1[random_id].clone()
                    self.gf1[random_id] = gf1[idx]
                    tmp_gf2 = self.gf2[random_id].clone()
                    self.gf2[random_id] = gf2[idx]
                    return_images.append(tmp)
                    return_gf1.append(tmp_gf1)
                    return_gf2.append(tmp_gf2)

                else:
                    return_images.append(image)
                    return_gf1.append(gf1[idx])
                    return_gf2.append(gf2[idx])
        return_images = Variable(torch.cat(return_images, 0))
        return_gf1 = Variable(torch.cat(return_gf1, 0))
        return_gf2 = Variable(torch.cat(return_gf2, 0))

        return return_images,return_gf1,return_gf2
