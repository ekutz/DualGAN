import numpy as np
import torch
from torch.nn.functional import adaptive_avg_pool2d
from fid.inception import InceptionV3
from scipy import linalg
import socket

DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro-2".lower()) else False


def calculate_frechet_distance(mu1, sigma1, mu2, sigma2, eps=1e-6):
    """Numpy implementation of the Frechet Distance.
    The Frechet distance between two multivariate Gaussians X_1 ~ N(mu_1, C_1)
    and X_2 ~ N(mu_2, C_2) is
            d^2 = ||mu_1 - mu_2||^2 + Tr(C_1 + C_2 - 2*sqrt(C_1*C_2)).

    Stable version by Dougal J. Sutherland.

    Params:
    -- mu1   : Numpy array containing the activations of a layer of the
               inception net (like returned by the function 'get_predictions')
               for generated samples.
    -- mu2   : The sample mean over activations, precalculated on an
               representative data set.
    -- sigma1: The covariance matrix over activations for generated samples.
    -- sigma2: The covariance matrix over activations, precalculated on an
               representative data set.

    Returns:
    --   : The Frechet Distance.
    """

    mu1 = np.atleast_1d(mu1.copy())
    mu2 = np.atleast_1d(mu2)

    sigma1 = np.atleast_2d(sigma1.copy())
    sigma2 = np.atleast_2d(sigma2)

    assert mu1.shape == mu2.shape, \
        'Training and test mean vectors have different lengths'
    assert sigma1.shape == sigma2.shape, \
        'Training and test covariances have different dimensions'

    diff = mu1 - mu2

    # Product might be almost singular
    covmean, _ = linalg.sqrtm(sigma1.dot(sigma2), disp=False)
    if not np.isfinite(covmean).all():
        msg = ('fid calculation produces singular product; '
               'adding %s to diagonal of cov estimates') % eps
        print(msg)
        offset = np.eye(sigma1.shape[0]) * eps
        covmean = linalg.sqrtm((sigma1 + offset).dot(sigma2 + offset))

    # Numerical error might give slight imaginary component
    if np.iscomplexobj(covmean):
        if not np.allclose(np.diagonal(covmean).imag, 0, atol=1e-3):
            m = np.max(np.abs(covmean.imag))
            raise ValueError('Imaginary component {}'.format(m))
        covmean = covmean.real

    tr_covmean = np.trace(covmean)

    return (diff.dot(diff) + np.trace(sigma1) +
            np.trace(sigma2) - 2 * tr_covmean)


class FID:
    def __init__(self, origin_numpy, dims=2048, batch_size=50, cuda=''):
        if len(origin_numpy.shape) == 3:
            self.origin_data = origin_numpy[:, np.newaxis]
        else:
            self.origin_data = origin_numpy
        if self.origin_data.shape[1] == 1:
            self.origin_data = np.repeat(self.origin_data, 3, axis=1)

        self.dims = dims
        self.cuda = cuda
        self.batch_size = batch_size

        block_idx = InceptionV3.BLOCK_INDEX_BY_DIM[self.dims]
        if not DEBUG:
            self.model = InceptionV3([block_idx])
            if cuda:
                self.model.cuda()
        self.m1, self.s1 = self.calculate_activation_statistics(self.origin_data)

    def get_activations_from_numpy(self, files_np):
        """
        @param files_np:
        """
        self.model.eval()
        if self.batch_size > len(files_np):
            print(('Warning: batch size is bigger than the data size. '
                   'Setting batch size to data size'))
            batch_size = len(files_np)
        else:
            batch_size = self.batch_size

        pred_arr = np.empty((len(files_np), self.dims))
        for i in range(0, len(files_np), batch_size):
            start = i
            end = i + batch_size
            images = files_np[start:end]
            # Reshape to (n_images, 3, height, width)
            # images = images.transpose((0, 3, 1, 2)) -> shape already fits req.
            # images /= 255

            batch = torch.from_numpy(images).type(torch.FloatTensor)
            if self.cuda:
                batch = batch.cuda()

            pred = self.model(batch)[0]

            # If model output is not scalar, apply global spatial average pooling.
            # This happens if you choose a dimensionality not equal 2048.
            if pred.size(2) != 1 or pred.size(3) != 1:
                pred = adaptive_avg_pool2d(pred, output_size=(1, 1))

            pred_arr[start:end] = pred.cpu().data.numpy().reshape(pred.size(0), -1)
        return pred_arr

    def calculate_activation_statistics(self, files):
        """Calculation of the statistics used by the FID.
        """

        if len(files.shape) == 3:
            files = files[:, np.newaxis]
        elif len(files.shape) == 4:
            if files.shape[-1] == 3:
                files = np.rollaxis(files, 3, 1)
        if files.shape[1] == 1:
            files = np.repeat(files, 3, axis=1)

        act = self.get_activations_from_numpy(files)
        mu = np.mean(act, axis=0)
        sigma = np.cov(act, rowvar=False)
        return mu, sigma


if __name__ == '__main__':
    print('start')
    fid = FID(np.array([1, 2, 2]))
    print('done')
