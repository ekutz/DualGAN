#!/bin/sh
### General options
### –- specify queue --
#BSUB -q gpuv100
### -- set the job Name --
#BSUB -J fid_test
### -- ask for number of cores (default: 1) --
#BSUB -n 1
### -- Select the resources: 1 gpu in exclusive process mode --
#BSUB -gpu "num=1:mode=exclusive_process"
### -- set walltime limit: hh:mm --  maximum 24 hours for GPU-queues right now
#BSUB -W 02:00
# request 5GB of system-memory
#BSUB -R "rusage[mem=16GB]"
# select the amount of GPU memory needed
#BSUB -R "select[gpu32gb]"
### -- set the email address --
# please uncomment the following line and put in your e-mail address,
# if you want to receive e-mail notifications on a non-default address
##BSUB -u s182902@student.dtu.dk
### -- send notification at start --
#BSUB -B
### -- send notification at completion--
#BSUB -N
### -- Specify the output and error file. %J is the job-id --
### -- -o and -e mean append, -oo and -eo mean overwrite --
#BSUB -o ../../logs/gpu-%J.out
#BSUB -e ../../logs/gpu_%J.err
# -- end of LSF options --

nvidia-smi
# Load the cuda module
module load cuda/10.2
module load python3/3.7.5

/appl/cuda/10.2/samples/NVIDIA_CUDA-10.2_Samples/bin/x86_64/linux/release/deviceQuery

python3 ./fid_score.py ../../../Datasets/Messidor/Base11_anno/256 ../../../Datasets/Messidor/Base12_anno/256
python3 ./fid_score.py ../../../Datasets/Messidor/Base11_anno/256 ../../../Datasets/Messidor/Base13_anno/256
python3 ./fid_score.py ../../../Datasets/Messidor/Base11_anno/256 ../../../Datasets/Messidor/Base14_anno/256
python3 ./fid_score.py ../../../Datasets/Messidor/Base11_anno/256 ../../../Datasets/Messidor/Base21_anno/256
python3 ./fid_score.py ../../../Datasets/Messidor/Base11_anno/256 ../../../Datasets/Messidor/Base22_anno/256