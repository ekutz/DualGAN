from datetime import datetime
import argparse


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def get_main_values():
    parser = argparse.ArgumentParser()
    parser.add_argument('--stage', type=str, default='1', choices=['1', '2', 'test'])
    parser.add_argument('--mode', type=str, default='train', choices=['train', 'optimize', 'inference'])
    parser.add_argument('--dataset', type=str, default='gland', choices=['gland', 'drive', 'messidor', 'mnist'])

    # Set model
    parser.add_argument('--Stage1_model', type=str, default='DCGAN', choices=['DCGAN', 'SAGAN'])
    parser.add_argument('--Stage2_model', type=str, default='pix2pix', choices=['pix2pix'])

    # Settings for training
    parser.add_argument('--epochs', type=int, default=100)
    parser.add_argument('--image_size', type=int, default=128)
    parser.add_argument('--batch_size', type=int, default=50)
    parser.add_argument('--normalize', type=str2bool, default=True)
    parser.add_argument('--center_mean', type=str2bool, default=True)

    # Load pretrained model

    # Basic settings for Stage1 training
    parser.add_argument('--adv_loss', type=str, default='wgan-gp', choices=['wgan-gp', 'hinge'])
    parser.add_argument('--g_lr', type=float, default=0.0001)
    parser.add_argument('--d_lr', type=float, default=0.0002)
    parser.add_argument('--real_label', type=float, default=1.0)
    parser.add_argument('--fake_label', type=float, default=0.0)
    parser.add_argument('--df_dim', type=int, default=64)
    parser.add_argument('--gf_dim', type=int, default=64)
    parser.add_argument('--z_dim', type=int, default=100)
    parser.add_argument('--split_data', type=float, default=1.0)
    # Extra settings for Stage 1
    parser.add_argument('--flip_labels', type=str2bool, default=False)
    parser.add_argument('--flip_interval', type=int, default=4)
    parser.add_argument('--label_smoothing', type=str2bool, default=False)
    # replace fully connected layers with convolutional layers to make network deep
    parser.add_argument('--deep', type=str2bool, default=False)
    parser.add_argument('--update_twice', type=str2bool, default=False)

    # Basic settings for Stage2 training
    parser.add_argument('--lr', type=float, default=0.0002)
    parser.add_argument('--data_aug', type=str2bool, default=True)
    parser.add_argument('--gen', type=str, default='Unet', choices=['Unet', 'ResUnet', 'ResUnet_a'])
    parser.add_argument('--patch_size', type=int, default=30, choices=[14, 30, 62])
    parser.add_argument('--ndf', type=int, default=32)
    parser.add_argument('--ngf', type=int, default=64)
    # PAN settings
    parser.add_argument('--pan_lambdas', nargs='+', type=float, default=[5.0, 1.0, 1.0, 1.0, 5.0],
                        help='lambdas of PAN_loss')
    parser.add_argument('--pan_mergin_m', type=int, default=50, help='positive margin of PAN loss')
    parser.add_argument('--loss', type=str, default='pixel', choices=['pixel', 'pan'])
    parser.add_argument('--relativistic', type=str2bool, default=True)
    parser.add_argument('--content_loss_factor', type=float, default=1e-2)
    parser.add_argument('--gan_loss_factor', type=float, default=5e-3)
    # log settings
    parser.add_argument('--log', type=str2bool, default=True)
    parser.add_argument('--sample', type=str2bool, default=True)
    parser.add_argument('--save_model', type=str2bool, default=True)

    parser.add_argument('--log_step', type=int, default=100)
    parser.add_argument('--sample_step', type=int, default=100)
    parser.add_argument('--model_save_step', type=int, default=0)
    parser.add_argument('--fid_step', type=int, default=1)
    # bay opt settings
    parser.add_argument('--max_evals', type=int, default=20)

    # inference settings
    parser.add_argument('--gen_amount', type=int, default=1)  # amount of to be generated images in inference
    parser.add_argument('--model_path', type=str, default=None)
    parser.add_argument('--gpu', type=str2bool, default=True)
    parser.add_argument('--inf_source', type=str, default=None)
    parser.add_argument('--inf_out', type=str, default=None)

    parser.add_argument('--hp_path',type=str, default='')
    parser.add_argument('--pool_size', type=int, default=0)
    return parser.parse_args()


def get_parameters_stage_one(parser):
    now = datetime.now()
    return {'model': str('sagan'),
            'adv_loss': str('wgan-gp'),
            'img_size': int(parser.image_size),
            'g_num': int(5),
            'z_dim': int(100),
            'g_conv_dim': int(128),
            'd_conv_dim': int(128),
            'lambda_gp': float(parser.batch_size),
            'input_channel': int(1),
            'version': str('00_') + now.strftime("%d%m%Y_%H%M%S"),
            'total_step': int(parser.epochs),
            'd_iters': float(5),
            'num_workers': int(2),
            'lr_decay': float(0.95),
            'beta1': float(0.5),
            'beta2': float(0.999),
            'pretrained_model': None,
            'train': True,
            'parallel': False,
            'dataset': str(parser.dataset),
            'use_tensorboard': False,
            'attn_path': str(parser.dataset + '/attn' + str(parser.image_size)),
            'fake_path': str(parser.dataset + '/individual_fake/' + str(parser.image_size)),
            }


def get_parameters_stage_two(parser):
    now = datetime.now()
    return {"in_channels": int(1),
            "out_channels": int(3),
            "epoch": int(0),
            "dataset": parser.dataset,
            "batch_size": parser.batch_size,
            'img_size': int(parser.image_size),
            "lr": float(0.0002),
            "b1": float(0.5),
            "b2": float(0.999),
            "decay_epoch": int(100),
            "n_cpu": int(8),
            "image_height": parser.image_size,
            "image_width": parser.image_size,
            'version': str('00_') + now.strftime("%d%m%Y_%H%M%S"),
            'train_image_path': str('./data/'),
            'model_save_path': str(parser.dataset + '/models/' + str(parser.image_size)),
            'sample_path': str(parser.dataset + '/samples/' + str(parser.image_size)),
            'attn_path': str(parser.dataset + '/attn' + str(parser.image_size)),
            'fake_path': str(parser.dataset + '/individual_fake/' + str(parser.image_size)),
            'log_path': str(parser.dataset + '/logs/' + str(parser.image_size)),
            'train': True,
            }

    # os.makedirs("Stage2/images/%s" % opt["dataset"], exist_ok=True)
    # os.makedirs("Stage2/saved_models/%s" % opt["dataset"], exist_ok=True)

    # size_train = (opt["image_height"], opt["image_width"])
