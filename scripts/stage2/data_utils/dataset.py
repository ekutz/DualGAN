from torchvision import transforms, utils
from torch.utils.data import Dataset


class CustomTensorDataset(Dataset):
    """TensorDataset with support of transforms.
    """

    def __init__(self, tensors, transform=None, normalize=False, mean=None, std=None,
                 img_trans=transforms.Compose([])):
        if std is None:
            std = [0.5, 0.5, 0.5]
        if mean is None:
            mean = [0.5, 0.5, 0.5]
        assert all(tensors[0].size(0) == tensor.size(0) for tensor in tensors)
        self.tensors = tensors
        self.transform = transform
        self.normalize = normalize
        self.norm = transforms.Compose([transforms.Normalize(mean=mean, std=std)])

    def __getitem__(self, index):
        x = self.tensors[0][index]
        y = self.tensors[1][index]
        if self.transform:
            x = self.transform(x)
        x = self.norm(x)

        return x, y

    def __len__(self):
        return self.tensors[0].size(0)


class CustomTensorDatasetPairedImages(Dataset):
    """TensorDataset with support of transforms.
    """

    def __init__(self, tensors, transform=None, mean=None, std=None):
        if std is None:
            std = [0.5, 0.5, 0.5]
        if mean is None:
            mean = [0.5, 0.5, 0.5]
        self.tensors = tensors
        self.transform = transform
        self.norm = transforms.Compose([transforms.Normalize(mean=mean, std=std)])

    def __getitem__(self, index):
        x = self.tensors[0][index]
        y = self.tensors[1][index]

        if self.transform:
            # joint transformations
            x, y = self.transform(x, y)
        x = self.norm(x)
        return x, y

    def __len__(self):
        return self.tensors[0].size(0)
