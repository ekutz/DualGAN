from stage2.Stage2_training import TrainerTwo
from stage2.Stage2_training_PAN import TrainerTwo as TrainerTwoPAN
from stage2.data_utils.utils import ObjectView
from fid.fid import calculate_frechet_distance
import torch
import socket

DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro".lower()) else False

def bayopt_objective(args):

    args_temp = ObjectView(args)
    dt_val = args_temp.data_loader_val
    dt_tr = args_temp.data_loader_train
    fid = args_temp.fid
    del args_temp.data_loader_val
    del args_temp.data_loader_train
    del args_temp.fid
    # create model based on given values
    trainer = TrainerTwo(dt_tr, dt_val, args_temp.config, args_temp.mean, args_temp.std, args_temp)
    # run training
    fake_images_np = trainer.train()
    # calculate activation statistics based on numpy image batch
    if not DEBUG:
        m2, s2 = fid.calculate_activation_statistics(fake_images_np)
        # calculate fid score by comparing it with the
        fid_score = calculate_frechet_distance(fid.m1, fid.s1, m2, s2)
        del m2, s2, fake_images_np
    else:
        fid_score = 0
    del fid
    del trainer
    del args_temp

    torch.cuda.empty_cache()
    return fid_score


def objective_bayopt_PAN(args):
    """Objective function for Bayesian optimization
    """
    args_temp = ObjectView(args)
    dt_val = args_temp.data_loader_val
    dt_tr = args_temp.data_loader_train
    fid = args_temp.fid
    del args_temp.data_loader_val
    del args_temp.data_loader_train
    del args_temp.fid
    # create model based on given values
    trainer = TrainerTwoPAN(dt_tr, dt_val, args_temp.config, args_temp.mean, args_temp.std, args_temp)
    # run training
    fake_images_np = trainer.train()
    # calculate activation statistics based on numpy image batch
    if not DEBUG:
        m2, s2 = fid.calculate_activation_statistics(fake_images_np)
        # calculate fid score by comparing it with the
        fid_score = calculate_frechet_distance(fid.m1, fid.s1, m2, s2)
        del m2, s2, fake_images_np
    else:
        fid_score = 0
    del fid
    del trainer
    del args_temp

    torch.cuda.empty_cache()
    # TODO remove print statement -> only for debugging on GPU
    return fid_score
