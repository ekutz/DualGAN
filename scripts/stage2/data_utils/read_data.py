from skimage.io import imread
from skimage.transform import resize
import os
from stage2.data_utils.utils import masks_to_numpy


def read_fake_with_filenames(path='./', image_size=64):
    size = (image_size, image_size)
    images = []
    names = []
    print(path)
    _, _, files = next(os.walk(path))
    for f in files:
        if f.endswith('.bmp'):
            images.append(
                resize(imread(os.path.join(path, f), plugin='pil', as_gray=True),
                       size,
                       mode='constant',
                       anti_aliasing=True)>0
            )
            names.append(os.path.splitext(f))
    return masks_to_numpy(images), names


def read_gland(path='gland/', image_size=64, read_images=True, read_masks=True):
    size_train = (image_size, image_size)
    images_train, masks_train = [], []
    root, dirs, _ = next(os.walk(path))
    for d in dirs:
        if d == 'train_B':
            _, _, files = next(os.walk(os.path.join(root, d)))
            for f in files:
                if not f.endswith('anno.bmp') and not f.endswith('csv'):  # and f.startswith('train'):
                    img_path = os.path.join(root, d, f)
                    mask_path = os.path.join(root, 'train_A', f)
                    if os.path.exists(mask_path):
                        if read_images:
                            images_train.append(resize(imread(img_path),
                                                       size_train,
                                                       mode='constant',
                                                       anti_aliasing=True))
                        if read_masks:
                            masks_train.append(resize(imread(mask_path, plugin='pil'),
                                                      size_train,
                                                      mode='constant',
                                                      anti_aliasing=True) > 0)
    if read_masks and not read_images:
        return masks_to_numpy(masks_train)
    if read_images and not read_masks:
        return masks_to_numpy(images_train)
    return masks_to_numpy(images_train), masks_to_numpy(masks_train)


def read_drive(path='retina', image_size=64, read_images=True, read_masks=True):
    size_train = (image_size, image_size)
    images_train, masks_train = [], []
    root, dirs, _ = next(os.walk(path))
    for d in dirs:
        if d == 'images':
            _, _, files = next(os.walk(os.path.join(root, d)))
            files = sorted(files)
            for f in files:
                img_path = os.path.join(root, d, f)
                mask_path = os.path.join(root, '1st_manual', os.path.splitext(f)[0].split('_')[0] + '_manual1.gif')
                if os.path.exists(mask_path):
                    if read_images:
                        images_train.append(resize(imread(img_path),
                                                   size_train,
                                                   mode='constant',
                                                   anti_aliasing=True))
                    if read_masks:
                        masks_train.append(resize(imread(mask_path, plugin='pil'),
                                                  size_train,
                                                  mode='constant',
                                                  anti_aliasing=True) > 0)
    if read_masks and not read_images:
        return masks_to_numpy(masks_train)
    if read_images and not read_masks:
        return masks_to_numpy(images_train)
    return masks_to_numpy(images_train), masks_to_numpy(masks_train)
