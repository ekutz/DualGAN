from torchvision import transforms
from torch.utils.data import Dataset, DataLoader

from stage2.data_utils.compose import *
import torchvision.datasets as datasets


class CustomTensorDataset(Dataset):
    """TensorDataset with support of transforms.
    """

    def __init__(self, tensors, img_trans=transforms.Compose([])):
        self.tensors = tensors
        self.img_trans = img_trans

    def __getitem__(self, index):
        x = self.tensors[0][index]
        y = self.tensors[1][index]
        x = self.img_trans(x)
        return x, y

    def __len__(self):
        return self.tensors[0].size(0)


class CustomTensorDatasetPairedImages(Dataset):
    """TensorDataset with support of transforms.
    """

    def __init__(self, tensors, transform=None, normalize=False, img_trans=transforms.Compose([])):
        # assert all(tensors[0].size(0) == tensor.size(0) for tensor in tensors)
        self.tensors = tensors
        self.transform = transform
        self.normalize = normalize
        self.norm_image = transforms.Compose([transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])])
        self.norm_mask = transforms.Compose([transforms.Normalize(mean=[0.5], std=[0.5])])

        self.img_trans = img_trans

    def __getitem__(self, index):
        x = self.tensors[0][index]
        y = self.tensors[1][index]
        # individual transformations on the image (e.g cropping of image to correct dimensions -see messidor)
        x = self.img_trans(x)

        if self.transform:
            # joint transformations
            x, y = self.transform(x, y)
        if self.normalize:
            x = self.norm_image(x)
            y = self.norm_mask(y)

        return x, y

    def __len__(self):
        return self.tensors[0].size(0)


def create_dataloader_fake_masks(images, mean, std, args):
    img_trans = transforms.Compose([
        # transforms.Normalize(mean=[mean], std=[std])
    ])
    train = CustomTensorDataset(tensors=((torch.from_numpy(np.rollaxis(images, 3, 1)),
                                          torch.from_numpy(np.stack(np.ones(len(images)))))),
                                img_trans=img_trans)
    return DataLoader(train, batch_size=args.batch_size, shuffle=False, pin_memory=True)


def get_mnist_dataloader(batch_size, image_size):
    dataset = datasets.MNIST(root='~/Datasets/images/', train=True, download=True,
                             transform=transforms.Compose([
                                 transforms.Resize(image_size),
                                 transforms.CenterCrop(image_size),
                                 transforms.ToTensor(),
                                 transforms.Normalize([0.5], [0.5]),
                             ]))
    return DataLoader(dataset, batch_size=batch_size, shuffle=True, pin_memory=True)


def create_dataloader_masks(train_data, args, mean, std):
    if args.normalize:
        img_trans = transforms.Compose([
            transforms.ToPILImage(),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.ToTensor(),
            transforms.Normalize(mean=[mean], std=[std])
        ])
    else:
        img_trans = transforms.Compose([
            transforms.ToPILImage(),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.ToTensor(),
        ])

    train = CustomTensorDataset(tensors=(torch.from_numpy(train_data[:, np.newaxis]),
                                         torch.from_numpy(np.stack(np.ones(len(train_data))))),
                                img_trans=img_trans)
    return DataLoader(train, batch_size=args.batch_size, shuffle=True, pin_memory=True)


def create_dataloader_paired_images(images, masks, args, train_test_ratio=None, data_aug=False, shuffle=True):
    if train_test_ratio:
        ix = np.random.choice(len(images), len(images), False)
        # print(ix)
        tr, val = np.split(ix, [int(len(images) * train_test_ratio)])  # set better values
    else:
        tr = np.arange(len(images))
    if data_aug:
        transform = Compose([ToPILImage(),
                             RandomHorizontalFlip(),
                             RandomVerticalFlip(),
                             ToTensor()
                             ])
    else:
        transform = Compose([])
    if args.dataset=='messidor':
        transform_image = transforms.Compose([transforms.ToPILImage(),
                                          transforms.CenterCrop(960),
                                          transforms.Resize(args.image_size),
                                          transforms.ToTensor()
                                          ])
    else:
        transform_image = transforms.Compose([])
    # for future implementations it could be important to

    train = CustomTensorDatasetPairedImages(
        tensors=(torch.from_numpy(np.rollaxis(images[tr], 3, 1)), torch.from_numpy(masks[tr, np.newaxis])),
        transform=transform,
        normalize=True,
        img_trans=transform_image
    )
    data_tr = DataLoader(train, batch_size=args.batch_size, shuffle=shuffle, pin_memory=True)
    if train_test_ratio:
        validation = CustomTensorDatasetPairedImages(
            tensors=(torch.from_numpy(np.rollaxis(images[val], 3, 1)), torch.from_numpy(masks[val, np.newaxis])),
            normalize=True,
            img_trans=transform_image
        )
        data_val = DataLoader(validation, batch_size=args.batch_size, shuffle=False, pin_memory=True)

        return data_tr, data_val
    else:
        return data_tr
