from __future__ import division
import torch
import random
from PIL import Image
import numpy as np
from scipy import ndimage, misc
import torchvision
import collections
import sys

if sys.version_info < (3, 3):
    Sequence = collections.Sequence
    Iterable = collections.Iterable
else:
    Sequence = collections.abc.Sequence
    Iterable = collections.abc.Iterable
try:
    import accimage
except ImportError:
    accimage = None
import numbers

# import torch.nn.functional as F
import torchvision.transforms.functional as f

NEAREST = NONE = 0
BILINEAR = LINEAR = 2
BICUBIC = CUBIC = 3
AFFINE = 0


def _get_image_size(img):
    if f._is_pil_image(img):
        return img.size
    elif isinstance(img, torch.Tensor) and img.dim() > 2:
        return img.shape[-2:][::-1]
    else:
        raise TypeError("Unexpected type {}".format(type(img)))


class Compose(object):
    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, img, mask):
        for t in self.transforms:
            img, mask = t(img, mask)
        return img, mask


class RandomRotation(object):
    def __init__(self, degrees, resample=False, expand=False, center=None, fill=(0, 0, 0)):
        if isinstance(degrees, numbers.Number):
            if degrees < 0:
                raise ValueError("If degrees is a single number, it must be positive.")
            self.degrees = (-degrees, degrees)
        else:
            if len(degrees) != 2:
                raise ValueError("If degrees is a sequence, it must be of len 2.")
            self.degrees = degrees

        self.resample = resample
        self.expand = expand
        self.center = center
        self.fill_img = (0, 0, 0)
        self.fill_mask = (255, 0, 0)

    def __call__(self, img, mask):
        angle = self.get_params(self.degrees)
        return f.rotate(img, angle, self.resample, self.expand, self.center, self.fill_img), f.rotate(mask, angle,
                                                                                                      self.resample,
                                                                                                      self.expand,
                                                                                                      self.center,
                                                                                                      self.fill_mask)

    @staticmethod
    def get_params(degrees):
        """Get parameters for ``rotate`` for a random rotation.

        Returns:
            sequence: params to be passed to ``rotate`` for random rotation.
        """
        angle = random.uniform(degrees[0], degrees[1])

        return angle


class RandomHorizontalFlip(object):
    def __call__(self, img, mask):
        if random.random() < 0.5:
            return f.vflip(img), f.vflip(mask)
        return img, mask


class RandomVerticalFlip(object):
    def __call__(self, img, mask):
        if random.random() < 0.5:
            return f.hflip(img), f.hflip(mask)
        return img, mask

    def __repr__(self):
        return self.__class__.__name__ + '()'


class NumpyToTensor(object):
    def __call__(self, img, mask):
        return torch.from_numpy(np.rollaxis(img, 2, 0)), torch.from_numpy(np.rollaxis(mask, 2, 0))


class ToTensor(object):
    def __call__(self, img, mask):
        return f.to_tensor(img), f.to_tensor(mask)

    def __repr__(self):
        return self.__class__.__name__ + '()'


class DeleteLastTwoMaskChannel(object):
    def __call__(self, img):
        return img[:1]


class FillMaskToThreeChannel(object):
    def __call__(self, img):
        img[img > 0] = 1
        img_temp = torch.zeros((3, img.shape[1], img.shape[2]), dtype=torch.float32)
        img_temp[0, :, :][img[0] == 1] = 1
        return img_temp


class ToPILImage(object):
    def __init__(self, mode=None):
        self.mode = mode

    def __call__(self, img, mask):
        return f.to_pil_image(img, self.mode), f.to_pil_image(mask, self.mode)

    def __repr__(self):
        format_string = self.__class__.__name__ + '('
        if self.mode is not None:
            format_string += 'mode={0}'.format(self.mode)
        format_string += ')'
        return format_string


class ToNumpy(object):
    def __call__(self, img, mask):
        return img.permute(1, 2, 0).numpy(), mask.permute(1, 2, 0).numpy()


class RandomCrop(object):
    def __init__(self, size: int, padding=None, pad_if_needed=False, fill=0, padding_mode='constant'):
        if isinstance(size, numbers.Number):
            self.size = (size, size)
        else:
            self.size = size
        self.padding = padding
        self.pad_if_needed = pad_if_needed
        self.fill = fill
        self.padding_mode = padding_mode

    @staticmethod
    def get_params(img, output_size):
        w, h = _get_image_size(img)
        th, tw = output_size
        if w == tw and h == th:
            return 0, 0, h, w

        i = random.randint(0, h - th)
        j = random.randint(0, w - tw)
        return i, j, th, tw

    def __call__(self, img, mask):

        if self.padding is not None:
            img = f.pad(img, self.padding, self.fill, self.padding_mode)

        # pad the width if needed
        if self.pad_if_needed and img.size[0] < self.size[1]:
            img = f.pad(img, (self.size[1] - img.size[0], 0), self.fill, self.padding_mode)
        # pad the height if needed
        if self.pad_if_needed and img.size[1] < self.size[0]:
            img = f.pad(img, (0, self.size[0] - img.size[1]), self.fill, self.padding_mode)

        i, j, h, w = self.get_params(img, self.size)
        _, _, h_m, w_m = self.get_params(img, self.size)
        assert h == h_m and w == w_m
        return f.crop(img, i, j, h, w), f.crop(mask, i, j, h, w)

    def __repr__(self):
        return self.__class__.__name__ + '(size={0}, padding={1})'.format(self.size, self.padding)


class UnNormalize(object):
    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    def __call__(self, tensor):
        """
        Args:
            tensor (Tensor): Tensor image of size (C, H, W) to be normalized.
        Returns:
            Tensor: Normalized image.
        """
        for t, m, s in zip(tensor, self.mean, self.std):
            t.mul_(s).add_(m)
            # The normalize code -> t.sub_(m).div_(s)
        return tensor


class NormalizeInverse(torchvision.transforms.Normalize):
    """
    Undoes the normalization and returns the reconstructed images in the input domain.
    """

    def __init__(self, mean, std):
        mean = torch.as_tensor(mean)
        std = torch.as_tensor(std)
        std_inv = 1 / (std + 1e-7)
        mean_inv = -mean * std_inv
        super().__init__(mean=mean_inv, std=std_inv)

    def __call__(self, tensor):
        return super().__call__(tensor.clone())


def _is_pil_image(img):
    if accimage is not None:
        return isinstance(img, (Image.Image, accimage.Image))
    else:
        return isinstance(img, Image.Image)


class Resize(object):
    """Resize the input PIL Image to the given size.

    Args:
        size (sequence or int): Desired output size. If size is a sequence like
            (h, w), output size will be matched to this. If size is an int,
            smaller edge of the image will be matched to this number.
            i.e, if height > width, then image will be rescaled to
            (size * height / width, size)
        interpolation (int, optional): Desired interpolation. Default is
            ``PIL.Image.BILINEAR``
    """

    def __init__(self, size, interpolation=Image.BILINEAR):
        assert isinstance(size, int) or (isinstance(size, Iterable) and len(size) == 2)
        self.size = size
        self.interpolation = interpolation

    def __call__(self, img, mask):
        """
        Args:
            img (PIL Image): Image to be scaled.

        Returns:
            PIL Image: Rescaled image.
        """
        return f.resize(img, self.size, self.interpolation), f.resize(mask, self.size, self.interpolation)
