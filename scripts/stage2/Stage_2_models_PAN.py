import torch
import torch.nn as nn
from torch.nn import functional as F
from torch.autograd import Variable


def upsample_cat(left_input, bottom_input):
    upsample = nn.Upsample(int(bottom_input.shape[-1] * 2))  # double bottom image dimensions
    bottom_input = upsample(bottom_input)
    return torch.cat([bottom_input, left_input], 1)


def merge(left_input, bottom_input):
    return torch.cat([bottom_input, left_input], 1)


class PSPModule(nn.Module):
    def __init__(self, features, out_features=1024, sizes=(1, 2, 3, 6)):
        super().__init__()
        self.stages = []
        self.stages = nn.ModuleList([self._make_stage(features, size) for size in sizes])
        self.bottleneck = nn.Conv2d(features * (len(sizes) + 1), out_features, kernel_size=1)
        self.relu = nn.ReLU()

    def _make_stage(self, features, size):
        prior = nn.AdaptiveAvgPool2d(output_size=(size, size))
        conv = nn.Conv2d(features, features, kernel_size=1, bias=False)
        return nn.Sequential(prior, conv)

    def forward(self, feats):
        h, w = feats.size(2), feats.size(3)
        priors = [F.upsample(input=stage(feats), size=(h, w), mode='bilinear', align_corners=True) for stage in
                  self.stages] + [feats]
        bottle = self.bottleneck(torch.cat(priors, 1))
        return self.relu(bottle)


class ResUNet_a(nn.Module):
    """
    Hybrid solution of resnet blocks and double conv blocks
    """

    def __init__(self, out_classes=3):
        super(ResUNet_a, self).__init__()
        self.conv_first = nn.Conv2d(1, 32, kernel_size=1)

        self.down_conv1 = ResBlock_a(32, 32, diletion=[1, 3, 15, 31])
        self.down_conv2 = ResBlock_a(32, 64, diletion=[1, 3, 15, 31])
        self.down_conv3 = ResBlock_a(64, 128, diletion=[1, 3, 15])
        self.down_conv4 = ResBlock_a(128, 256, diletion=[1, 3, 15])
        self.down_conv5 = ResBlock_a(256, 512, diletion=[1])
        self.down_conv6 = ResBlock_a(512, 1024, diletion=[1])

        # self.double_conv = DoubleConv(512, 1024)
        self.bottleneck = PSPModule(1024, out_features=1024)

        self.up_conv5 = UpBlock_a(512 + 1024, 256, diletion=[1])
        # self.up_conv5 = UpBlock_a(512 + 512, 512, diletion=[1])
        self.up_conv4 = UpBlock_a(256 + 256, 256, diletion=[1, 3, 15])
        self.up_conv3 = UpBlock_a(128 + 256, 128, diletion=[1, 3, 15])
        self.up_conv2 = UpBlock_a(64 + 128, 64, diletion=[1, 3, 15, 31])
        self.up_conv1 = UpBlock_a(64 + 32, 32, diletion=[1, 3, 15, 31])
        self.end = PSPModule(64, out_features=64)

        self.conv_last = nn.Conv2d(64, out_classes, kernel_size=1)
        self.th = nn.Tanh()

    def forward(self, x):
        first = self.conv_first(x)
        x, skip1_out = self.down_conv1(first)
        x, skip2_out = self.down_conv2(x)
        x, skip3_out = self.down_conv3(x)
        x, skip4_out = self.down_conv4(x)
        x, skip5_out = self.down_conv5(x)
        _, x = self.down_conv6(x)
        # x = self.double_conv(x)
        x = self.bottleneck(x)
        # x = self.up_conv6(x, skip6_out)
        x = self.up_conv5(x, skip5_out)
        x = self.up_conv4(x, skip4_out)
        x = self.up_conv3(x, skip3_out)
        x = self.up_conv2(x, skip2_out)
        x = self.up_conv1(x, skip1_out)
        x = self.end(torch.cat([x, first], dim=1))
        x = self.conv_last(x)

        return self.th(x)


class ResUNet(nn.Module):
    """
    Hybrid solution of resnet blocks and double conv blocks
    """

    def __init__(self, out_classes=3):
        super(ResUNet, self).__init__()

        self.down_conv1 = ResBlock(1, 64)
        self.down_conv2 = ResBlock(64, 128)
        self.down_conv3 = ResBlock(128, 256)
        self.down_conv4 = ResBlock(256, 512)
        self.down_conv5 = ResBlock(512, 512)

        self.double_conv = DoubleConv(512, 1024)

        self.up_conv5 = UpBlock(512 + 1024, 512)
        self.up_conv4 = UpBlock(512 + 512, 512)
        self.up_conv3 = UpBlock(256 + 512, 256)
        self.up_conv2 = UpBlock(128 + 256, 128)
        self.up_conv1 = UpBlock(64 + 128, 64)

        self.conv_last = nn.Conv2d(64, out_classes, kernel_size=1)
        self.th = nn.Tanh()

    def forward(self, x):
        x, skip1_out = self.down_conv1(x)
        x, skip2_out = self.down_conv2(x)
        x, skip3_out = self.down_conv3(x)
        x, skip4_out = self.down_conv4(x)
        x, skip5_out = self.down_conv5(x)
        x = self.double_conv(x)
        x = self.up_conv5(x, skip5_out)
        x = self.up_conv4(x, skip4_out)
        x = self.up_conv3(x, skip3_out)
        x = self.up_conv2(x, skip2_out)
        x = self.up_conv1(x, skip1_out)
        x = self.conv_last(x)

        return self.th(x)


class DownBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(DownBlock, self).__init__()
        self.double_conv = DoubleConv(in_channels, out_channels)
        self.down_sample = nn.MaxPool2d(2)

    def forward(self, x):
        skip_out = self.double_conv(x)
        down_out = self.down_sample(skip_out)
        return (down_out, skip_out)


class UpBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(UpBlock, self).__init__()
        self.up_sample = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
        self.double_conv = DoubleConv(in_channels, out_channels)

    def forward(self, down_input, skip_input):
        x = self.up_sample(down_input)
        x = torch.cat([x, skip_input], dim=1)
        return self.double_conv(x)


class UpBlock_a(nn.Module):
    def __init__(self, in_channels, out_channels, diletion):
        super(UpBlock_a, self).__init__()
        self.up_sample = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
        # self.double_conv = DoubleConv(in_channels, out_channels)
        self.res_block = ResBlock_a(in_channels=in_channels, out_channels=out_channels, diletion=diletion)

    def forward(self, down_input, skip_input):
        x = self.up_sample(down_input)
        x = torch.cat([x, skip_input], dim=1)
        _, x = self.res_block(x)
        return x


class DoubleConv(nn.Module):
    def __init__(self, in_channels, out_channels, dilation=1):
        super(DoubleConv, self).__init__()
        self.double_conv = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=dilation, dilation=dilation),
            # nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(out_channels, out_channels, kernel_size=3, padding=dilation, dilation=dilation),
            # nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True),
        )

    def forward(self, x):
        return self.double_conv(x)


class ResBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(ResBlock, self).__init__()
        self.downsample = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, kernel_size=1, stride=1, bias=False),
            # nn.BatchNorm2d(out_channels)
            )
        self.double_conv = DoubleConv(in_channels, out_channels)
        self.down_sample = nn.MaxPool2d(2)
        self.relu = nn.ReLU()

    def forward(self, x):
        identity = self.downsample(x)
        out = self.double_conv(x)
        out = self.relu(out + identity)
        return self.down_sample(out), out


class ResBlock_a(nn.Module):
    def __init__(self, in_channels, out_channels, diletion=None):
        super(ResBlock_a, self).__init__()
        if diletion is None:
            diletion = [1]
        self.diletion = diletion
        self.downsample = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, kernel_size=1, stride=1, bias=False),
            # nn.BatchNorm2d(out_channels)
        )

        if len(diletion) >= 1:
            self.double_conv_1 = DoubleConv(in_channels, out_channels, dilation=diletion[0])
        if len(diletion) >= 2:
            self.double_conv_2 = DoubleConv(in_channels, out_channels, dilation=diletion[1])
        if len(diletion) >= 3:
            self.double_conv_3 = DoubleConv(in_channels, out_channels, dilation=diletion[2])
        if len(diletion) >= 4:
            self.double_conv_4 = DoubleConv(in_channels, out_channels, dilation=diletion[3])

        self.down_sample = nn.MaxPool2d(2)
        self.relu = nn.ReLU()

    def forward(self, x):
        identity = self.downsample(x)
        if len(self.diletion) >= 1:
            out1 = self.double_conv_1(x)
            temp_out = out1 + identity
        if len(self.diletion) >= 2:
            out2 = self.double_conv_2(x)
            temp_out += out2
        if len(self.diletion) >= 3:
            out3 = self.double_conv_3(x)
            temp_out += out3
        if len(self.diletion) >= 4:
            out4 = self.double_conv_4(x)
            temp_out += out4
        out = self.relu(temp_out)
        return self.down_sample(out), out


class Generator(nn.Module):
    """
    Generator model based on UNet
    """

    def __init__(self, batch_size, image_size=64, out_c=3, ngf=64):
        self.out_c = out_c
        self.image_size = image_size
        super().__init__()
        self.momentum = 0.05
        self.en1 = self.encode_block(1, ngf, drop_rate=0.0)
        self.en2 = self.encode_block(ngf, ngf * 2, drop_rate=0.0)
        self.en3 = self.encode_block(ngf * 2, ngf * 4, drop_rate=0.0)
        self.en4 = self.encode_block(ngf * 4, ngf * 8, drop_rate=0.0)
        self.en5 = self.encode_block(ngf * 8, ngf * 8, drop_rate=0.0)
        self.en6 = self.encode_block(ngf * 8, ngf * 8, drop_rate=0.0)
        self.en7 = self.encode_block(ngf * 8, ngf * 8, drop_rate=0.0)

        self.boNe = self.encode_block(ngf * 8, ngf * 8, drop_rate=0.)

        self.de8 = self.decode_block(ngf * 8, ngf * 8, drop_rate=0.5, merged_input=False)
        if self.image_size == 256:
            self.de7 = self.decode_block(ngf * 8, ngf * 8, drop_rate=0.5, merged_input=False)
        else:
            self.de7 = self.decode_block(ngf * 8, ngf * 8, drop_rate=0.5)

        self.de6 = self.decode_block(ngf * 8, ngf * 8, drop_rate=0.5)
        self.de5 = self.decode_block(ngf * 8, ngf * 8, drop_rate=0.)

        self.de4 = self.decode_block(ngf * 8, ngf * 8, drop_rate=0.)
        self.de3 = self.decode_block(ngf * 8, ngf * 4, drop_rate=0.)
        self.de2 = self.decode_block(ngf * 4, ngf * 2, drop_rate=0.)
        self.de1 = self.decode_block(ngf * 2, ngf, ll=True)

        self.pool = nn.MaxPool2d(2, 2)
        self.th = nn.Tanh()

    def encode_block(self, input_channel, output_channel, drop_rate=0., kernel=4, stride=2):
        en_list = []
        en_list.extend([
            nn.Conv2d(input_channel, output_channel, kernel_size=kernel, padding=1, stride=stride),
            # nn.BatchNorm2d(output_channel, momentum=self.momentum),
            nn.LeakyReLU(0.2)
        ])
        if drop_rate > 0:
            en_list += [nn.Dropout(drop_rate)]
        return nn.Sequential(*en_list)

    def decode_block(self, input_channel, output_channel, ll=False, drop_rate=0., merged_input=True):
        if merged_input:
            de_list = [nn.ConvTranspose2d(input_channel + output_channel, output_channel, 4, padding=1, stride=2)]
        else:
            de_list = [nn.ConvTranspose2d(input_channel, output_channel, 4, padding=1, stride=2)]

        de_list.extend([
            # nn.BatchNorm2d(output_channel, momentum=self.momentum),
            nn.LeakyReLU()
        ])
        if drop_rate > 0.:
            de_list += [nn.Dropout(drop_rate)]
        if ll:
            de_list.append(nn.Conv2d(output_channel, self.out_c, 3, padding=1))
        return nn.Sequential(*de_list)

    def forward(self, x):
        e1 = self.en1(x)
        e2 = self.en2(e1)
        e3 = self.en3(e2)
        e4 = self.en4(e3)
        e5 = self.en5(e4)
        e6 = self.en6(e5)
        if self.image_size == 512:

            e7 = self.en7(e6)
            b = self.boNe(e7)
            d8 = self.de8(b)
            d7 = merge(e7, d8)
            d7 = self.de7(d7)

        else:
            b = self.boNe(e6)
            d7 = self.de7(b)

        d6 = merge(e6, d7)
        d6 = self.de6(d6)
        d5 = merge(e5, d6)
        d5 = self.de5(d5)
        d4 = merge(e4, d5)
        d4 = self.de4(d4)
        d3 = merge(e3, d4)
        d3 = self.de3(d3)
        d2 = merge(e2, d3)
        d2 = self.de2(d2)
        d1 = merge(e1, d2)
        d1 = self.de1(d1)

        return self.th(d1)


class Discriminator(nn.Module):
    """
    Discriminator model -  Patch GAN Discriminator
    TODO: first and last layer have stride of 1 instead of 2 - see original implementation stated wrong in the paper
    TODO: aim to have a 70x70 patch GAN
    """

    def __init__(self, image_size=64, input_channel=4, ndf=32, patch_size=30):
        super().__init__()

        '''
        TODO outsource fixed parameter to settings in the beginning
        '''
        self.image_size = image_size

        layer = []
        channel = []
        if self.image_size == 64:
            if patch_size == 14:
                channel = [input_channel, ndf, ndf * 2, ndf * 4, 1]
            elif patch_size == 30:
                channel = [input_channel, ndf * 2, ndf * 4, 1]
            elif patch_size == 62:
                channel = [input_channel, ndf * 4, 1]
        elif self.image_size == 128:
            if patch_size == 14:
                channel = [input_channel, ndf, ndf * 2, ndf * 4, ndf * 8, 1]
            elif patch_size == 30:
                channel = [input_channel, ndf * 2, ndf * 4, ndf * 8, 1]
            elif patch_size == 62:
                channel = [input_channel, ndf * 4, ndf * 8, 1]
        elif self.image_size == 256:
            if patch_size == 14:
                channel = [input_channel, ndf, ndf * 2, ndf * 4, ndf * 8, ndf * 16, 1]
            elif patch_size == 30:
                channel = [input_channel, ndf * 2, ndf * 4, ndf * 8, ndf * 16, 1]
            elif patch_size == 62:
                channel = [input_channel, ndf * 4, ndf * 8, ndf * 16, 1]
        elif self.image_size == 512:
            if patch_size == 14:
                channel = [input_channel, ndf, ndf * 2, ndf * 4, ndf * 8, ndf * 16, ndf * 32, 1]
            elif patch_size == 30:
                channel = [input_channel, ndf * 2, ndf * 4, ndf * 8, ndf * 16, ndf * 32, 1]
            elif patch_size == 62:
                channel = [input_channel, ndf * 4, ndf * 8, ndf * 16, ndf * 32, 1]

        def conv_block(no, in_ch, out_ch, last=False, stride=2):  # no: Number of layer, in/out: input/output channels
            if not last:
                inter_conv = nn.Conv2d(in_ch, out_ch,
                                       kernel_size=4,
                                       stride=stride,
                                       padding=1
                                       )
                layer.extend([inter_conv,
                              nn.LeakyReLU(0.2)])
                inter_conv.register_forward_hook(self.add_intermediate_output)
            else:
                conv = nn.Conv2d(in_ch, out_ch,
                                 kernel_size=4,
                                 padding=1)
                layer.extend([
                    # nn.ZeroPad2d((1, 0, 1, 0)),
                    conv
                ])
                conv.register_forward_hook(self.add_intermediate_output)

        for i, in_ch in enumerate(channel[:-1]):
            if i < len(channel) - 3:
                conv_block(i, in_ch, channel[i + 1])
            elif i == len(channel) - 3:
                conv_block(i, in_ch, channel[i + 1], stride=1)
            else:
                conv_block(i, in_ch, channel[i + 1], True)

        self.layer = nn.Sequential(*layer)
        self.sm = nn.Sigmoid()

    # def forward(self, img_A, img_B):
    def forward(self, x):
        self.intermediate_outputs = []
        # Concatenate image and condition image by channels to produce input
        # x = torch.cat((img_A, img_B), 1)
        x = self.layer(x)
        # print(x.shape)
        x = self.sm(x)
        return x

    def add_intermediate_output(self, conv, input, output):
        self.intermediate_outputs.append(Variable(output.data, requires_grad=False))

    def get_intermediate_outputs(self):
        return self.intermediate_outputs
