from torch.backends import cudnn
from torch.utils.tensorboard import SummaryWriter
from collections import OrderedDict
from hyperopt import fmin, tpe, Trials, hp
from stage2.data_utils.objective import *
from stage2.data_utils.dataloader import *
from stage2.data_utils.read_data import *
from stage2.config import *
from fid.fid import FID
# from memory_profiler import profile
import pickle
import cv2
from functools import partial

print(socket.gethostname())
DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro".lower()) else False

if DEBUG:
    DATASET_DIR_GLAND = '../../../../Datasets/Warwick_QU_Dataset/train'
    DATASET_DIR_DRIVE = '/Users/eike/Datasets/DRIVE/training'
    DATASET_DIR_MESSIDOR = '/Users/eike/Datasets/Messidor'
else:
    DATASET_DIR_GLAND = '../Datasets/Warwick_QU_Dataset/train'
    DATASET_DIR_DRIVE = '../Datasets/DRIVE/training'
    DATASET_DIR_MESSIDOR = '../Datasets/Messidor/'

manualSeed = 999
print("Random Seed: ", manualSeed)
random.seed(manualSeed)
np.random.seed(manualSeed)
torch.manual_seed(manualSeed)


def run_trials(space, args):
    """
    Run a number of trials, save after a defined number of iterations.
    """
    trials_step = 0  # how many additional trials to do after loading saved trials. 1 = save after iteration
    max_evals = args.max_evals  # initial max_evals.

    try:
        trials = pickle.load(open(args.dataset+'params.hyperopt', 'rb'))
        max_evals = len(trials.trials) + trials_step
        print('Continuing from {} trials to {} (+{}) trials'.format(len(trials.trials), max_evals, trials_step))
    except:
        trials = Trials()
        print('Starting new trials...')

    print('Starting new trials...')
    if args.loss == 'pixel':
        best = fmin(bayopt_objective, space, algo=partial(tpe.suggest, n_startup_jobs=3),
                    max_evals=max_evals, trials=trials)
    elif args.loss == 'pan':
        best = fmin(objective_2_PAN, space, algo=partial(tpe.suggest, n_startup_jobs=3),
                    max_evals=max_evals, trials=trials)

    print("Best:", best)
    with SummaryWriter(log_dir='runs_stage2_bayopt/' + args.now.strftime("%b%d_%H-%M-%S") + '/') as w:
        for i in range(len(trials.results)):
            hyperparams = {}
            for key in trials.vals.keys():
                if isinstance(trials.vals[key][i], np.int64):
                    hyperparams[key] = int(trials.vals[key][i])
                elif isinstance(trials.vals[key][i], np.float64):
                    hyperparams[key] = float(trials.vals[key][i])
                else:
                    hyperparams[key] = trials.vals[key][i]
            w.add_hparams(hyperparams,
                          {'hparam/FID': trials.results[i]['loss']})
    # save the trials object
    with open(args.dataset+'params.hyperopt', 'wb') as f:
        pickle.dump(trials, f)
    return best


def preMask(mask, mb=3, th=0.5):
    mask = np.array([cv2.medianBlur(im, mb) for im in mask])
    mask[mask > th] = 1
    mask[mask <= th] = 0
    # kernel = np.ones((3, 3), np.uint8)
    # mask = np.array(cv2.dilate(mask, kernel, iterations=1))
    #
    # return mask[:, :, np.newaxis]
    return mask
    # y_train[y_train > 0] = 1


def read_data_two(args):
    # read data
    if args.dataset == 'gland':
        return read_gland(DATASET_DIR_GLAND,
                          image_size=args.image_size)
    elif args.dataset == 'drive':
        return read_drive(DATASET_DIR_DRIVE,
                          image_size=args.image_size)
    elif args.dataset == 'messidor':
        return read_messidor2(DATASET_DIR_MESSIDOR,
                              image_size=args.image_size,
                              read_images=True,
                              read_masks=True)
    return np.array(), np.array()


def main(args, config):
    # For fast training
    cudnn.benchmark = True
    # ==========STAGE2==========
    if args.mode == 'train' or args.mode == 'optimize':
        image_data, masks_data = read_data_two(args)
        print(
            "Loaded dataset: " + config['dataset'] + "\n" +
            "\tLoaded images: " + str(len(image_data)) + "\n" +
            "\tLoaded masks: " + str(len(masks_data))
        )
        if args.split_data < 1.0:
            if DEBUG:
                ix = np.random.choice(40, 40, False)
            else:
                if not args.dataset == 'drive':
                    PIK = "random_list.dat"
                    with open(PIK, "rb") as f:
                        ix = np.array(pickle.load(f))
                else:
                    ix = np.arange(20)
            # ix = np.random.choice(len(image_data), len(image_data), False)
            tr, val = np.split(ix, [int(len(image_data) * args.split_data)])  # set better values
            image_data = image_data[tr]
            masks_data = masks_data[tr]
        print(
            "Loaded dataset: " + config['dataset'] + "\n" +
            "\tSelected images: " + str(len(image_data)) + "\n" +
            "\tSelected masks: " + str(len(masks_data))
        )
        # Data loader
        if args.dataset == 'drive':
            dl_tr = create_dataloader_paired_images(images=image_data[:15],
                                                    masks=masks_data[:15],
                                                    args=args,
                                                    data_aug=False)
            dl_val = create_dataloader_paired_images(images=image_data[15:],
                                                     masks=masks_data[15:],
                                                     args=args,
                                                     data_aug=False,
                                                     shuffle=False)
        else:
            dl_tr, dl_val = create_dataloader_paired_images(images=image_data,
                                                            masks=masks_data,
                                                            args=args,
                                                            train_test_ratio=0.75)
        mean, std = 0.5, 0.5
        # train model
        if not DEBUG:
            fid = FID(image_data)
        else:
            fid = None

    space = OrderedDict([
        ('stage', '2'),
        ('mode', 'optimize'),
        ('dataset', args.dataset),
        ('epochs', args.epochs),
        ('image_size', args.image_size),
        ('batch_size', args.batch_size),
        ('gen', args.gen),
        ('lr', hp.loguniform('lr', np.log(0.00001), np.log(0.001))),
        ('ngf', hp.choice('ngf', [8, 16, 32, 64])),
        ('ndf', hp.choice('ndf', [8, 16, 32, 64])),
        ('patch_size', hp.choice('patch_size', [14, 30, 62])),
        ('real_label', 1),
        ('fake_label', 0),
        ('normalize', args.normalize),
        ('center_mean', False),
        ('flip_labels', False),
        ('flip_interval', 3),
        ('log', False),
        ('sample', False),
        ('save_model', False),
        ('log_step', 0),
        ('sample_step', 0),
        ('model_save_step', 0),
        ('fid_step', args.fid_step),
        ('mean', mean),
        ('std', std),
        ('config', config),
        ('fid', fid),
        ('data_loader_train', dl_tr),
        ('data_loader_val', dl_val),
        ('model_path', None),
        ('relativistic', args.relativistic),
        ('gan_loss_factor', hp.loguniform('gan_loss_factor', np.log(0.00001), np.log(0.1))),
        ('content_loss_factor', hp.loguniform('content_loss_factor', np.log(0.00001), np.log(0.1)))

    ])
    # trials = Trials()
    # best = fmin(objective_2, space, algo=tpe.suggest, max_evals=args.max_evals, trials=trials)
    for i in range(1):
        best = run_trials(space, args)
        print(best)


if __name__ == '__main__':
    args = get_main_values()
    now = datetime.now()
    args.now = now

    config = {}
    if args.stage == '1':
        config = get_parameters_stage_one(args)
    elif args.stage == '2':
        config = get_parameters_stage_two(args)
    elif args.stage == 'test':
        exit()
    main(args, config)
