from torch.nn import MSELoss, L1Loss
import time
import datetime
import sys
import matplotlib.pyplot as plt
from stage2.data_utils.utils import *
from stage2.data_utils.loss import PerceptualLoss, VGGPerceptualLoss
from PIL import Image

from stage2.Stage_2_models_PAN import Generator, Discriminator, ResUNet, ResUNet_a
from fid.fid import calculate_frechet_distance
import math
import socket
from typing import Optional, Tuple

DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro".lower()) else False
MODEL_PATH = '../Models/s2/'
if DEBUG:
    IMAGE_SAVE_PATH = "/Users/eike/Datasets/Messidor/fake_256/"
else:
    IMAGE_SAVE_PATH = "../Datasets/Messidor/fake_256/"


def normalize(tensor, range: Optional[Tuple[int, int]] = None, scale_each: bool = False):
    tensor = tensor.clone()  # avoid modifying tensor in-place
    if range is not None:
        assert isinstance(range, tuple), \
            "range has to be a tuple (min, max) if specified. min and max are numbers"

    def norm_ip(img, min, max):
        img.clamp_(min=min, max=max)
        img.add_(-min).div_(max - min + 1e-5)

    def norm_range(t, range):
        if range is not None:
            norm_ip(t, range[0], range[1])
        else:
            norm_ip(t, float(t.min()), float(t.max()))

    if scale_each is True:
        for t in tensor:  # loop over mini-batch dimension
            norm_range(t, range)
    else:
        norm_range(tensor, range)
    return tensor


def weights_init_normal(m):
    classname = m.__class__.__name__
    if classname.find("Conv") != -1:
        torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find("BatchNorm2d") != -1:
        torch.nn.init.normal_(m.weight.data, 1.0, 0.02)
        torch.nn.init.constant_(m.bias.data, 0.0)


def save_image(image, path):
    # im = Image.fromarray((image * 255).astype(np.uint8))
    im = Image.fromarray(np.rollaxis((image * 255).astype(np.uint8), 0, 3))
    im = im.convert("RGB")
    im.save(path + '.png', "PNG")


class Pix2pixInference:
    def __init__(self, dataloader, filenames, args):
        self.args = args
        self.dataloader = dataloader
        self.filenames = filenames
        if args.gpu:
            self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        else:
            self.device = torch.device("cpu")
        # init generator
        if args.gen == 'ResUnet':
            # self.g = Generator_ResUnet(image_size=self.args.image_size)
            self.g = ResUNet()
        elif args.gen == 'ResUnet_a':
            self.g = ResUNet_a()
        else:
            self.g = Generator(batch_size=self.args.batch_size, image_size=self.args.image_size, ngf=self.args.ngf)
            # self.g = Generator(batch_size=self.args.batch_size, image_size=self.args.image_size)

        # load model
        if self.device.type == 'cpu':

            checkpoint = torch.load(args.model_path, map_location=lambda storage, location: storage)
        else:
            checkpoint = torch.load(args.model_path)
        print("Inference from step: ", checkpoint['step'])
        self.g.load_state_dict(checkpoint['netG_state_dict'])

    def inference(self):
        total_runs = int(math.ceil(self.args.gen_amount / self.args.batch_size))
        n = 0
        if self.args.inf_out:
            os.makedirs(os.path.join(self.args.inf_out, 'fake_256'), exist_ok=True)
        else:
            os.makedirs(IMAGE_SAVE_PATH, exist_ok=True)
        for idx, masks_batch in enumerate(self.dataloader):
            # get image and filename
            # pass to ge batch?!!!!
            image_batch = self.g(masks_batch[0])
            image_batch = normalize(image_batch)
            # check image in correct range
            # iterate over batch
            for idx2, image in enumerate(image_batch.detach().numpy()):
                if n >= self.args.gen_amount:
                    break
                # if DEBUG:
                #     plt.imshow(np.transpose(image, (1, 2, 0)))
                #     plt.show()
                # save image
                i = idx * self.args.batch_size + idx2
                filename = self.filenames[i]
                if self.args.inf_out:
                    save_image(image, os.path.join(self.args.inf_out, 'fake_256', filename))
                else:
                    save_image(image, os.path.join(IMAGE_SAVE_PATH, filename))
                n += 1


class TrainerTwo(object):
    STAGE = 'stage2'

    def __init__(self, data_loader_train, data_loader_val, config, mean, std, args, writer=None, fid=None):
        print('lr: ' + str(args.lr) + ' bs: ' + str(args.batch_size)
              + ' ngf: ' + str(args.ngf)
              + ' ndf: ' + str(args.ndf)
              + ' gan_loss_factor: ' + str(args.gan_loss_factor)
              + ' content_loss_factor: ' + str(args.content_loss_factor)
              )
        self.args = args
        self.writer = writer
        self.data_loader_train = data_loader_train
        self.data_loader_val = data_loader_val
        self.config = config
        self.mean = mean
        self.std = std
        self.fid = fid
        # self.unnorm = UnNormalize(self.mean, self.std)
        self.version = config['version']
        # self.model_save_step = self.args.model_save_step
        # Path
        self.log_path = os.path.join('./results', self.STAGE, config['log_path'], self.version)
        self.sample_path = os.path.join('./results', self.STAGE, config['sample_path'], self.version)
        # self.model_save_path = os.path.join('./results', self.STAGE, config['model_save_path'], self.version)

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        # build model
        # Loss functions
        self.criterion_GAN = MSELoss()
        self.perception_criterion = PerceptualLoss()
        self.perception_criterion2 = VGGPerceptualLoss()
        self.content_criterion = L1Loss()

        # Calculate output of image d (PatchGAN)
        # self.patch = (1, self.config["image_height"] // 2 ** 2, self.config["image_width"] // 2 ** 2)
        if self.args.patch_size == 14:
            self.patch = (1, 14, 14)
        elif self.args.patch_size == 30:
            self.patch = (1, 30, 30)
        elif self.args.patch_size == 62:
            self.patch = (1, 62, 62)
        # self.patch = (1, 32, 32)

        # Initialize g and d
        # self.g = Generator(batch_size=self.args.batch_size, image_size=self.args.image_size)
        if args.gen == 'ResUnet':
            # self.g = Generator_ResUnet(image_size=self.args.image_size)
            self.g = ResUNet()
        elif args.gen == 'ResUnet_a':
            self.g = ResUNet_a()
        else:
            self.g = Generator(batch_size=self.args.batch_size, image_size=self.args.image_size, ngf=self.args.ngf)
        self.d = Discriminator(image_size=self.args.image_size, ndf=self.args.ndf, patch_size=self.args.patch_size)

        # summary(self.g, (4, 256, 256))

        if str(self.device) == 'gpu':
            self.g = self.g.to(self.device)
            self.d = self.d.to(self.device)
            self.criterion_GAN.to(self.device)
            self.perception_criterion.to(self.device)
            self.perception_criterion2.to(self.device)
            self.content_criterion.to(self.device)
            self.tensor = torch.cuda.FloatTensor
        else:
            self.tensor = torch.FloatTensor
        print(self.device)
        # Optimizers
        self.optimizer_G = torch.optim.Adam(self.g.parameters(), lr=self.args.lr,
                                            betas=(self.config["b1"], self.config["b2"]))
        self.optimizer_D = torch.optim.Adam(self.d.parameters(), lr=self.args.lr,
                                            betas=(self.config["b1"], self.config["b2"]))
        if self.args.model_path:
            # Load pre-trained models
            if self.device.type == 'cpu':
                checkpoint = torch.load(args.model_path, map_location=lambda storage, location: storage)
            else:
                checkpoint = torch.load(args.model_path)
            self.g.load_state_dict(checkpoint['netG_state_dict'])
            self.d.load_state_dict(checkpoint['netD_state_dict'])
            self.optimizer_G.load_state_dict(checkpoint['optimizerG_state_dict'])
            self.optimizer_D.load_state_dict(checkpoint['optimizerD_state_dict'])
            self.start_epoch = checkpoint['epoch']
            self.step = checkpoint['step']

        else:
            # Initialize weights
            # self.g.apply(weights_init_normal)
            self.d.apply(weights_init_normal)
            self.start_epoch = 0
            self.step = 0

    def train(self):
        prev_time = time.time()
        end = self.start_epoch + self.args.epochs + 1
        self.log_real(epoch=self.step)

        for epoch in range(self.start_epoch, end):
            for i, (X_batch, Y_batch) in enumerate(self.data_loader_train):
                self.step += X_batch.shape[0]
                self.g.train()
                self.d.train()
                # Model inputs
                real_a = Variable(Y_batch.type(self.tensor))
                real_b = Variable(X_batch.type(self.tensor))
                fake_b = self.g(real_a)

                # Adversarial ground truths
                valid = Variable(self.tensor(np.ones((real_a.size(0), *self.patch))), requires_grad=False)
                fake = Variable(self.tensor(np.zeros((real_a.size(0), *self.patch))), requires_grad=False)

                # ---------------------
                #  Train Discriminator
                # ---------------------

                self.optimizer_D.zero_grad()

                # Real + fake loss
                # real_inters = self.d.get_intermediate_outputs()

                # Fake loss
                pred_real = self.d(torch.cat((real_b, real_a), 1))
                pred_fake = self.d(torch.cat((fake_b.detach(), real_a), 1))
                if self.args.relativistic:
                    discriminator_rf = pred_real - pred_fake.mean()
                    discriminator_fr = pred_fake - pred_real.mean()

                    loss_real = self.criterion_GAN(discriminator_rf, valid)
                    loss_fake = self.criterion_GAN(discriminator_fr, fake)
                else:
                    loss_real = self.criterion_GAN(pred_real, valid)
                    loss_fake = self.criterion_GAN(pred_fake, fake)
                # fake_inters = self.d.get_intermediate_outputs()

                # # calc Parceptual Adversarial Loss
                # loss_PAN = 0
                # for (fake_i, real_i, lam) in zip(fake_inters, real_inters, self.args.pan_lambdas):
                #     # print(fake_i.shape,real_i.shape,lam)
                #     loss_PAN += self.criterion_PAN(fake_i, real_i) * lam
                #
                # if loss_PAN.item() > self.args.pan_mergin_m:
                #     loss_PAN = Variable(self.tensor(np.array([0], dtype=np.float)), requires_grad=False)
                # else:
                #     loss_PAN = Variable(self.tensor(np.array([self.args.pan_mergin_m], dtype=np.float)),
                #                         requires_grad=False) - loss_PAN

                # Total loss
                loss_d = 0.5 * (loss_real + loss_fake)

                loss_d.backward()
                self.optimizer_D.step()
                # ------------------
                #  Train Generators
                # ------------------

                self.optimizer_G.zero_grad()

                # GAN loss
                pred_fake = self.d(torch.cat((fake_b, real_a), 1))
                # print(pred_fake.shape, valid.shape)
                if self.args.relativistic:
                    pred_real = self.d(torch.cat((real_b, real_a), 1))
                    discriminator_rf = pred_real - pred_fake.mean()
                    discriminator_fr = pred_fake - pred_real.mean()

                    loss_real = self.criterion_GAN(discriminator_rf, valid)
                    loss_fake = self.criterion_GAN(discriminator_fr, fake)
                    loss_gan = 0.5 * (loss_real + loss_fake)
                else:
                    loss_gan = self.criterion_GAN(pred_fake, valid)
                # outputs of intermediate layers
                loss_PAN = self.perception_criterion(real_b, fake_b)
                loss_PAN2 = self.perception_criterion2(real_b, fake_b)
                loss_content = self.content_criterion(fake_b, real_b)

                # Total loss
                # loss_g = loss_gan*5e-3 + loss_PAN + loss_content*1e-2
                loss_g = loss_gan * self.args.gan_loss_factor + loss_PAN + loss_content * self.args.content_loss_factor

                loss_g.backward()

                self.optimizer_G.step()

                # --------------
                #  Log Progress
                # --------------
                # Determine approximate time left
                batches_done = epoch * len(self.data_loader_train) + i
                batches_left = self.args.epochs * len(self.data_loader_train) - batches_done
                time_left = datetime.timedelta(seconds=batches_left * (time.time() - prev_time))
                prev_time = time.time()

                # Print log
                if self.args.mode == 'train':
                    sys.stdout.write(
                        "\r[Epoch %d/%d] [Batch %d/%d] [D loss: %f] [G loss: %f, pan: %f, adv: %f] ETA: %s"
                        % (
                            epoch,
                            self.args.epochs,
                            i,
                            len(self.data_loader_train),
                            loss_d.item(),
                            loss_g.item(),
                            loss_PAN.item(),
                            loss_gan.item(),
                            time_left,
                        )
                    )
                if self.args.mode == 'train':
                    if self.writer:
                        self.writer.add_scalar('Loss/loss_d', loss_d.item(), self.step)
                        self.writer.add_scalar('Loss/loss_g', loss_g.item(), self.step)
                        self.writer.add_scalar('Loss/loss_PAN', loss_PAN.item(), self.step)
                        self.writer.add_scalar('Loss/loss_PAN2', loss_PAN2.item(), self.step)

                        self.writer.add_scalar('Loss/loss_gan', loss_gan.item(), self.step)
                    if DEBUG:
                        self.sample_images(0)
                    if i % 4 == 0:
                        self.log_fake(epoch=self.step)
            if self.args.mode == 'train':

                if epoch % self.args.fid_step == 0:
                    if self.writer:
                        fake_images_np = self.get_gen_samples(no_samples=25)
                        fid_score = self.get_fid(fake_images_np)
                        self.writer.add_scalar('Metric/FID', fid_score, self.step)
            if self.args.model_save_step > 0 and epoch % self.args.model_save_step == 0 and epoch > 0:
                self.save_models(epoch, self.step)

        return self.get_gen_samples(no_samples=25)

    def save_models(self, epoch, step):
        # save_path = '{}/{}/epoch_{}.pt'.format(MODEL_PATH, self.args.now, epoch)
        save_path = os.path.join(MODEL_PATH, self.args.now.strftime("%b%d_%H-%M-%S"))
        # if not os.path.exists(save_path):
        os.makedirs(save_path, exist_ok=True)
        save_path = os.path.join(save_path, str(epoch) + '.pt')
        torch.save({
            'epoch': epoch,
            'step': step,
            'netG_state_dict': self.g.state_dict(),
            'netD_state_dict': self.d.state_dict(),
            'optimizerD_state_dict': self.optimizer_D.state_dict(),
            'optimizerG_state_dict': self.optimizer_G.state_dict(),
        }, save_path)

    def get_fid(self, fake_images_np):
        if not DEBUG:
            m2, s2 = self.fid.calculate_activation_statistics(fake_images_np)
            return calculate_frechet_distance(self.fid.m1, self.fid.s1, m2, s2)
        else:
            return 0

    def get_gen_samples(self, no_samples=100):
        """Calculates the FID score based on the given data distribution"""
        # Calculate the amount of needed runs to reach no_samples
        no_runs = math.ceil(no_samples / self.args.batch_size)
        # Run the generator until the amount of needed samples is reached
        for i in range(no_runs):
            _, y_batch = next(iter(self.data_loader_val))
            real_a = Variable(y_batch.type(self.tensor))
            fake_b = self.g(real_a)
            fake_images = torch.cat([fake_images, fake_b], dim=0) if 'fake_images' in locals() else fake_b

        # Reduce to the amount of needed samples
        if fake_images.shape[0] > no_samples:
            fake_images = fake_images[:no_samples]

        if self.args.normalize:
            fake_images_np = normalize(fake_images).detach().cpu().numpy()
            # fake_images_np = np.array(self.unnorm(var2tensor(fake_images.clone())))[:, 0, :, :]
        else:
            fake_images_np = fake_images.detach().cpu().numpy()
            # fake_images_np = np.array(var2tensor(fake_images.clone()))[:, 0, :, :]
        return fake_images_np

    def sample_images(self, batches_done):
        """Saves a generated sample from the validation set"""
        x_batch, y_batch = next(iter(self.data_loader_val))
        real_a = Variable(y_batch.type(self.tensor))
        real_b = Variable(x_batch.type(self.tensor))
        fake_b = self.g(real_a)
        real_a = normalize(real_a).detach().cpu().numpy()
        fake_b = normalize(fake_b).detach().cpu().numpy()
        real_b = normalize(real_b).detach().cpu().numpy()
        plt.figure()
        plt.subplot(1, 3, 1)
        plt.imshow(real_a[0, 0])
        plt.axis('off')
        plt.subplot(1, 3, 2)
        plt.imshow(np.rollaxis(fake_b[0], 0, 3))
        plt.axis('off')
        plt.subplot(1, 3, 3)
        plt.imshow(np.rollaxis(real_b[0], 0, 3))
        plt.axis('off')
        # plt.savefig(os.path.join(self.sample_path, '{}_fake.png'.format(batches_done + 1)), transparent=True, dpi=150)
        # plt.close()
        plt.show()

    def log_real(self, epoch):
        """Write log to tensorboard"""

        # Generate sample images and save them to tensorboard
        # self.g.eval()
        r = 4
        real_rgb = np.zeros((r, 3, self.args.image_size, self.args.image_size))
        real_mask = np.zeros((r, 1, self.args.image_size, self.args.image_size))
        n = 0
        dataloader_iter = iter(self.data_loader_val)
        while n < 4:
            x_batch, y_batch = next(dataloader_iter)
            real_a_tmp = Variable(y_batch.type(self.tensor))
            real_b_tmp = Variable(x_batch.type(self.tensor))

            real_b = normalize(real_b_tmp).detach().cpu().numpy() if n == 0 else np.concatenate(
                (real_b, normalize(real_b_tmp).detach().cpu().numpy()), axis=0)
            real_a = normalize(real_a_tmp).detach().cpu().numpy() if n == 0 else np.concatenate(
                (real_a, normalize(real_a_tmp).detach().cpu().numpy()), axis=0)
            n += x_batch.shape[0]

        for i in range(r):
            # Fake masks
            if self.args.normalize:
                img_real = real_b[i]
                real_rgb[i, 0] = img_real[0]
                real_rgb[i, 1] = img_real[1]
                real_rgb[i, 2] = img_real[2]
                mask_real = real_a[i]
                real_mask[i, 0] = mask_real[0]
        if self.writer:
            self.writer.add_images('real_rgb', real_rgb, epoch)
            self.writer.add_images('reak_mask', real_mask, epoch)

    def log_fake(self, epoch):
        """Write log to tensorboard"""

        # Generate sample images and save them to tensorboard
        # self.g.eval()
        if self.args.sample_step != 0 and epoch % self.args.sample_step == 0:

            r = 4
            fake_rgb = np.zeros((r, 3, self.args.image_size, self.args.image_size))
            n = 0
            dataloader_iter = iter(self.data_loader_val)
            while n < 4:
                x_batch, y_batch = next(dataloader_iter)
                real_a_tmp = Variable(y_batch.type(self.tensor))
                fake_b_tmp = self.g(real_a_tmp)

                fake_b = normalize(fake_b_tmp).detach().cpu().numpy() if n == 0 else np.concatenate(
                    (fake_b, normalize(fake_b_tmp).detach().cpu().numpy()), axis=0)
                n += x_batch.shape[0]

            for i in range(r):
                # Fake masks
                if self.args.normalize:
                    img_fake = fake_b[i]
                    fake_rgb[i, 0] = img_fake[0]
                    fake_rgb[i, 1] = img_fake[1]
                    fake_rgb[i, 2] = img_fake[2]
            if self.writer:
                self.writer.add_images('fake_rgb', fake_rgb, epoch)
